# General 

This is a module for the Diapason processing with OTB
The module must be used with OTB Orfeo Toolbox (https://www.orfeo-toolbox.org/). 
It is designed to work with OTB v6.2 (or higher).

This module can be built as a remote module or as a standalone module

# Getting Started 

## Installation

1. Clone the repository
```
%git clone
```

2. Go to source directory
```
%cd DiapOTBModule
```

3. Create build directory
```
% mkdir build
% cd build
```

4. Setup OTB environment

5. Configure, compile and install
```
% CC=${GCCHOME}/bin/gcc CXX=${GCCHOME}/bin/g++ cmake -DOTB_BUILD_MODULE_AS_STANDALONE=ON -DCMAKE_INSTALL_PREFIX=<install_directory> -DCMAKE_BUILD_TYPE=<Release or Debug> ..
% make
% make install
```

6. Configure with tests, compile and run tests
```
% CC=${GCCHOME}/bin/gcc CXX=${GCCHOME}/bin/g++ cmake -DBUILD_TESTING=ON -DOTB_BUILD_MODULE_AS_STANDALONE=ON -DCMAKE_INSTALL_PREFIX=<install_directory> -DCMAKE_BUILD_TYPE=<Release or Debug> ..
% make
% ctest
```

## Usage to launch one application (ex : SARMultiLook)

### C++ application

1. Load environment
```
export OTB_APPLICATION_PATH=<install_directory>/lib:$OTB_APPLICATION_PATH
export LD_LIBRARY_PATH=<install_directory>/lib/:$LD_LIBRARY_PATH
```
A file (diapOTBEnv.sh) was creating into install_directory during the installation. 
This file can be used to load the environment. Other environment variables can be 
setted such as OTB_GEOID_FILE or ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS.

2. Get help
```
otbApplicationLauncherCommandLine SARMultiLook
```

3. Execute
```
otbApplicationLauncherCommandLine SARMultiLookApp -in ./Data_In/sar/Reunion/S1A_S4_SLC__without_calibration.SAFE/measurement/s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002.tiff -out ./Data_Out/out_SAR_Diap/s1a-s4-ml-vv.tif -mlran 3 -mlazi 3 -mlgain 0.1
```

### Python

1. Load environment
```
export OTB_APPLICATION_PATH=<install_directory>/lib:$OTB_APPLICATION_PATH
export LD_LIBRARY_PATH=<install_directory>/lib/:$LD_LIBRARY_PATH
```
A file (diapOTBEnv.sh) was creating into install_directory during the installation. 
This file can be used to load the environment. Other environment variables can be 
setted such as OTB_GEOID_FILE or ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS.


2. Write a Python file
```
import otbApplication as otb
app = otb.Registry.CreateApplication("SARMultiLook")
app.SetParameterString("in", "s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002.tiff")
app.SetParameterString("out", "output.tif")
app.SetParameterInt("mlran",3)
app.SetParameterInt("mlazi",3)
app.SetParameterFloat("mlgain", 0.1)
app.ExecuteAndWriteOutput()
```

3. Execute
```
python <script_python.py>
```

## Usage to launch DiapOTB processing

1. Load environment
```
source <install_directory>/diapOTBEnv.sh
```
Other environment variables can be setted such as OTB_GEOID_FILE or 
ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS.

2. Configure your processing with a configuration file
```
Example into <install_directory>/python_src/ex_config.ini
```

3. Launch the processing
```
python $DIAPOTB_INSTALL/python_src/diapOTB.py <configuration_file>
```
