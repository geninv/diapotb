/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARStreamingGridInformationImageFilter.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{
namespace Wrapper
{

class SARGridStatistics : public Application
{
public:
  typedef SARGridStatistics Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARGridStatistics, otb::Wrapper::Application);

  // Filters
  typedef otb::SARStreamingGridInformationImageFilter<FloatVectorImageType> FilterType;
  

private:
  void DoInit() override
  {
    SetName("SARGridStatistics");
    SetDescription("Computes grid statistics.");

    SetDocLongDescription("This application computes some statistics for a "
      "deformation grid such as minimum, maximum shift or mean shift on each"
      " dimension.");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "ingrid",   "Input grid (Vector Image)");
    SetParameterDescription("ingrid", "Input Grid Vector Image (Shift_ran, Shift_azi).");
   
    AddParameter(ParameterType_Float, "threshold", "Threshold for computation of mean shift (used only if grid components are superior or equal to 3)");
    SetParameterDescription("threshold","Threshold for computation of mean shift (used only if grid components are superior or equal to 3).");
    SetDefaultParameterFloat("threshold", 0.3);
    SetMinimumParameterFloatValue("threshold", 0.0001);
    MandatoryOff("threshold");

    // Parameter Outputs
    AddParameter(ParameterType_Float,"minran","Minumum shift on range (output of this application)");
    SetParameterDescription("minran", "Minumum shift on range.");
    SetParameterRole("minran", Role_Output);
    SetDefaultParameterFloat("minran", 0);

    AddParameter(ParameterType_Float,"minazi","Minumum shift on azimut (output of this application)");
    SetParameterDescription("minazi", "Minumum shift on azimut.");
    SetParameterRole("minazi", Role_Output);
    SetDefaultParameterFloat("minazi", 0);

    AddParameter(ParameterType_Float,"maxran","Maximum shift on range (output of this application)");
    SetParameterDescription("maxran", "Maximum shift on range.");
    SetParameterRole("maxran", Role_Output);
    SetDefaultParameterFloat("maxran", 0);

    AddParameter(ParameterType_Float,"maxazi","Maximum shift on azimut (output of this application)");
    SetParameterDescription("maxazi", "Maximum shift on azimut.");
    SetParameterRole("maxazi", Role_Output);
    SetDefaultParameterFloat("maxazi", 0);

    AddParameter(ParameterType_Float,"meanran","Mean shift on range (output of this application)");
    SetParameterDescription("meanran", "Mean shift on range.");
    SetParameterRole("meanran", Role_Output);
    SetDefaultParameterFloat("meanran", 0);

    AddParameter(ParameterType_Float,"meanazi","Mean shift on azimut (output of this application)");
    SetParameterDescription("meanazi", "Mean shift on azimut.");
    SetParameterRole("meanazi", Role_Output);
    SetDefaultParameterFloat("meanazi", 0);
    
 
    AddRAMParameter();

    SetDocExampleParameterValue("ingrid","./grid.tiff");
  }

  void DoUpdateParameters() override
  {
    // Nothing // TODO: o do here : all parameters are independent
  }

void DoExecute() override
{  
  // Get numeric parameters
  float threshold = GetParameterFloat("threshold");

  // Log information
  otbAppLogINFO(<<"Threshold for mean computation (only if the grid are a number of components sup or equal to 3)  :"<<threshold);
  
  // Get input grid
  FloatVectorImageType::Pointer GridPtr = GetParameterFloatVectorImage("ingrid");
  
  // Persistent Filter
  FilterType::Pointer filter = FilterType::New();
  m_Ref.push_back(filter.GetPointer());

  // Configure Filter
  filter->SetThreshold(threshold);
  filter->SetInput(GridPtr);
  filter->SetEstimateMean(true);

  // Get statistics values
  filter->Update();
  double meanRan, meanAzi;
  filter->GetMean(meanRan, meanAzi);
  double minRan, minAzi, maxRan, maxAzi; 
  filter->GetMinMax(minRan, minAzi, maxRan, maxAzi);

  // Assigne outputs
  SetParameterFloat("minran", minRan);
  SetParameterFloat("minazi", minAzi);
  SetParameterFloat("maxran", maxRan);
  SetParameterFloat("maxazi", maxAzi);
  SetParameterFloat("meanran", meanRan);
  SetParameterFloat("meanazi", meanAzi);

}
   // Vector for filters 
  std::vector<itk::ProcessObject::Pointer> m_Ref;
};


}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARGridStatistics)
