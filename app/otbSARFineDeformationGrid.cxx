/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperCompositeApplication.h"
#include "otbWrapperApplicationFactory.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{
namespace Wrapper
{

class SARFineDeformationGrid : public CompositeApplication
{
public:
  typedef SARFineDeformationGrid Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARFineDeformationGrid, otb::Wrapper::CompositeApplication);

private:
  void DoInit() override
  {
    // Clear all internal applications
    ClearApplications(); 	
       
    // Add the three internal applications
    AddApplication("SARCorrelationGrid", "CorGridApp", "Application for correlation grid creation");
    AddApplication("SARDEMGrid", "DEMGridApp", "Application for DEM grid creation");
    AddApplication("SARCorrectionGrid", "CorrectionGridApp", "Application for DEM grid correction by correlation grid");
     
    // SARFineDeformationGrid descripttion
    SetName("SARFineDeformationGrid");
    SetDescription("geo_grid step for Diapason chain.");

    SetDocLongDescription("This application executes the geo_grid step with "
      "three internal applications : SARCorrelationGrid, SARDEMGrid and "
      "SARCorrectionGrid. The aim is to obtain a fine deformation grid between"
      " master and slave SAR images.");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "indem",   "Input DEM");
    SetParameterDescription("indem", "DEM to extract DEM geometry.");

    AddParameter(ParameterType_InputImage,  "insarmaster",   "Input SAR Master image");
    SetParameterDescription("insarmaster", "SAR Master Image to extract SAR geometry.");

    AddParameter(ParameterType_InputImage,  "insarslave",   "Input SAR Slave image");
    SetParameterDescription("insarslave", "SAR Slave Image to extract SAR geometry.");

    AddParameter(ParameterType_InputImage,  "inmlmaster",   "Input Master Multilooked (real image)");
    SetParameterDescription("inmlmaster", "Master Image  Multilooked (real image).");

    AddParameter(ParameterType_InputImage,  "inmlslave",   "Input Slave Multilooked (real image)");
    SetParameterDescription("inmlslave", "Slave Image Multilooked (real image).");
    
    AddParameter(ParameterType_OutputImage,  "out", "Output Deformation grid (Vector image)");
    SetParameterDescription("out","Output Deformation Grid Vector Image (Shift_ran, Shift_azi).");
    
    AddRAMParameter();

    AddParameter(ParameterType_InputImage,  "indemprojmaster",   "Input vector of DEM projected into SAR Master geometry");
    SetParameterDescription("indemprojmaster", "Input vector of DEM projected into SAR Master geometry.");
    MandatoryOff("indemprojmaster");

    AddParameter(ParameterType_InputImage,  "indemprojslave",   "Input vector of DEM projected into SAR Slave geometry");
    SetParameterDescription("indemprojslave", "Input vector of DEM projected into SAR Slave geometry.");
    MandatoryOff("indemprojslave");
    

    AddParameter(ParameterType_Int, "mlran", "MultiLook factor on distance");
    SetParameterDescription("mlran","MultiLook factor on distance.");
    SetDefaultParameterInt("mlran", 3);
    SetMinimumParameterIntValue("mlran", 1);
    MandatoryOff("mlran");
    
    AddParameter(ParameterType_Int, "mlazi", "MultiLook factor on azimut");
    SetParameterDescription("mlazi","MultiLook factor on azimut.");
    SetDefaultParameterInt("mlazi", 3);
    SetMinimumParameterIntValue("mlazi", 1);
    MandatoryOff("mlazi");
    
    AddParameter(ParameterType_Int, "gridsteprange", "Grid step for range dimension (into SLC/SAR geometry)");
    SetParameterDescription("gridsteprange","Grid step for range dimension (into SLC/SAR geometry).");
    SetDefaultParameterInt("gridsteprange", 150);
    SetMinimumParameterIntValue("gridsteprange", 1);
    MandatoryOff("gridsteprange");

    AddParameter(ParameterType_Int, "gridstepazimut", "Grid step for azimut dimension (into SLC/SAR geometry)");
    SetParameterDescription("gridstepazimut","Grid step for azimut dimension (into SLC/SAR geometry).");
    SetDefaultParameterInt("gridstepazimut", 150);
    SetMinimumParameterIntValue("gridstepazimut", 1);
    MandatoryOff("gridstepazimut");

    AddParameter(ParameterType_Float, "threshold", "Threshold for correlation rate");
    SetParameterDescription("threshold","Threshold for correlation rate.");
    SetDefaultParameterFloat("threshold", 0.3);
    SetMinimumParameterFloatValue("threshold", 0.0001);
    MandatoryOff("threshold");
    
    AddParameter(ParameterType_Float, "gap", "Maximum difference between input grid values and mean values");
    SetParameterDescription("gap","Maximum difference between input grid values and mean values.");
    SetDefaultParameterFloat("gap", 0.7);
    SetMinimumParameterFloatValue("gap", 0.01);
    MandatoryOff("gap");

    AddParameter(ParameterType_String, "advantage", "Give an advantage to DEM or Correlation Grid");
    SetParameterDescription("advantage","Give an advantage to DEM or Correlation Grid.");
    MandatoryOff("advantage");
    
    SetDocExampleParameterValue("indem","S21E055.hgt");
    SetDocExampleParameterValue("insarmaster","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
    SetDocExampleParameterValue("inmlmaster","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_ML.tiff");
    SetDocExampleParameterValue("insarslave","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002_SLC.tiff");
    SetDocExampleParameterValue("inmlslave","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002_ML.tiff");
    SetDocExampleParameterValue("out","fineDeformationGrid.tiff");
 }

  void DoUpdateParameters() override
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute() override
  {
    // Get Input Images
    FloatImageType::Pointer inputDEM = GetParameterFloatImage("indem");
    ComplexFloatImageType::Pointer inputMasterSAR = GetParameterComplexFloatImage("insarmaster");
    ComplexFloatImageType::Pointer inputSlaveSAR = GetParameterComplexFloatImage("insarslave");
    FloatImageType::Pointer inputMasterML = GetParameterFloatImage("inmlmaster");
    FloatImageType::Pointer inputSlaveML = GetParameterFloatImage("inmlslave");


    // Get numeric parameters
    int factor_azi = GetParameterInt("mlazi");
    int factor_ran = GetParameterInt("mlran");
    int grid_step_azi = GetParameterInt("gridstepazimut");
    int grid_step_ran = GetParameterInt("gridsteprange");
    float threshold = GetParameterFloat("threshold");
    float gap = GetParameterFloat("gap");

    std::string advantage = GetParameterString("advantage");
    bool demGridToFavor = true;
    
    if (advantage == "correlation" || advantage == "cor" || advantage == "corgrid")
      {
	demGridToFavor = false;
      } 
     
    if (demGridToFavor)
      {
	otbAppLogINFO(<<"Opt in favor of projection (DEMGrid)");
      }
    else
      {
	otbAppLogINFO(<<"Opt in favor of correlation (CorGrid)");
      }


    // Execute the Pipelone with internal applications
    // First application : SARCorrelationGrid
    GetInternalApplication("CorGridApp")->SetParameterInputImage("inmaster", inputMasterML);
    GetInternalApplication("CorGridApp")->SetParameterInputImage("inslave", inputSlaveML);
    GetInternalApplication("CorGridApp")->SetParameterInt("mlran", factor_ran);
    GetInternalApplication("CorGridApp")->SetParameterInt("mlazi", factor_azi);
    GetInternalApplication("CorGridApp")->SetParameterInt("gridsteprange", grid_step_ran);
    GetInternalApplication("CorGridApp")->SetParameterInt("gridstepazimut", grid_step_azi);
    //if (demGridToFavor)
      {
	GetInternalApplication("CorGridApp")->SetParameterInt("nooffset", 1);
      }
    ExecuteInternal("CorGridApp");

    // Second application : SARDEMGrid
    GetInternalApplication("DEMGridApp")->SetParameterInputImage("indem", inputDEM);
    GetInternalApplication("DEMGridApp")->SetParameterInputImage("insarmaster", inputMasterSAR);
    GetInternalApplication("DEMGridApp")->SetParameterInputImage("insarslave", inputSlaveSAR);
    if (GetParameterByKey("indemprojmaster")->HasValue() && GetParameterByKey("indemprojslave")->HasValue())
    {
      FloatVectorImageType::Pointer DEMProjOnMasterPtr = GetParameterImage("indemprojmaster");
      FloatVectorImageType::Pointer DEMProjOnSlavePtr = GetParameterImage("indemprojslave");
      
      GetInternalApplication("DEMGridApp")->SetParameterInputImage("indemprojmaster", DEMProjOnMasterPtr);
      GetInternalApplication("DEMGridApp")->SetParameterInputImage("indemprojslave", DEMProjOnSlavePtr);
    }
    GetInternalApplication("DEMGridApp")->SetParameterInt("mlran", factor_ran);
    GetInternalApplication("DEMGridApp")->SetParameterInt("mlazi", factor_azi);
    GetInternalApplication("DEMGridApp")->SetParameterInt("gridsteprange", grid_step_ran);
    GetInternalApplication("DEMGridApp")->SetParameterInt("gridstepazimut", grid_step_azi);
    ExecuteInternal("DEMGridApp");
    
    // Third (and last) application : SARMultiLook
    GetInternalApplication("CorrectionGridApp")->SetParameterInputImage("indemgrid", 
			                 GetInternalApplication("DEMGridApp")->GetParameterOutputImage("out"));
    GetInternalApplication("CorrectionGridApp")->SetParameterInputImage("incorgrid", 
					GetInternalApplication("CorGridApp")->GetParameterOutputImage("out"));
    GetInternalApplication("CorrectionGridApp")->SetParameterFloat("threshold", threshold);
    GetInternalApplication("CorrectionGridApp")->SetParameterFloat("gap", gap);
    GetInternalApplication("CorrectionGridApp")->SetParameterString("advantage", advantage);
    ExecuteInternal("CorrectionGridApp");
    
    // Retrive the output of CorrectionGridApp
    SetParameterOutputImage("out", GetInternalApplication("CorrectionGridApp")->GetParameterOutputImage("out"));

  }
  // Vector for filters 
  std::vector<itk::ProcessObject::Pointer> m_Ref;

};


}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARFineDeformationGrid)
