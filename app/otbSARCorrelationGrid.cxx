/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "itkFFTNormalizedCorrelationImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"

#include "otbSARTemporalCorrelationGridImageFilter.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{
namespace Wrapper
{

class SARCorrelationGrid : public Application
{
public:
  typedef SARCorrelationGrid Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARCorrelationGrid, otb::Wrapper::Application);

  // Filters
  typedef itk::FFTNormalizedCorrelationImageFilter<FloatImageType, FloatImageType>          CorFilterType;
  typedef itk::MinimumMaximumImageCalculator< FloatImageType>			            MinMaxCalculatorType;
  typedef otb::SARTemporalCorrelationGridImageFilter<FloatImageType, FloatVectorImageType>  CorGridFilterType;
  

private:
  void DoInit() override
  {
    SetName("SARCorrelationGrid");
    SetDescription("Computes SAR correlation shift (into temporal domain).");

    SetDocLongDescription("This application computes correlation shifts between two images : "
    "shift in range and shift in azimut. "
    "The inputs of this application are MultiLooked images (real images).");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "inmaster",   "Input Master image (real image)");
    SetParameterDescription("inmaster", "Master Image (real image).");

    AddParameter(ParameterType_InputImage,  "inslave",   "Input Slave image (real image)");
    SetParameterDescription("inslave", "Slave Image (real image).");

    AddParameter(ParameterType_Int, "mlran", "MultiLook factor on distance");
    SetParameterDescription("mlran","MultiLook factor on distance.");
    SetDefaultParameterInt("mlran", 3);
    SetMinimumParameterIntValue("mlran", 1);
    MandatoryOff("mlran");
    
    AddParameter(ParameterType_Int, "mlazi", "MultiLook factor on azimut");
    SetParameterDescription("mlazi","MultiLook factor on azimut.");
    SetDefaultParameterInt("mlazi", 3);
    SetMinimumParameterIntValue("mlazi", 1);
    MandatoryOff("mlazi");

    AddParameter(ParameterType_Int, "gridsteprange", "Grid step for range dimension (into SLC/SAR geometry)");
    SetParameterDescription("gridsteprange","Grid step for range dimension (into SLC/SAR geometry).");
    SetDefaultParameterInt("gridsteprange", 150);
    SetMinimumParameterIntValue("gridsteprange", 1);
    MandatoryOff("gridsteprange");

    AddParameter(ParameterType_Int, "gridstepazimut", "Grid step for azimut dimension (into SLC/SAR geometry)");
    SetParameterDescription("gridstepazimut","Grid step for azimut dimension (into SLC/SAR geometry).");
    SetDefaultParameterInt("gridstepazimut", 150);
    SetMinimumParameterIntValue("gridstepazimut", 1);
    MandatoryOff("gridstepazimut");

    AddParameter(ParameterType_Int, "nooffset", "Set 0 to offset of first Line and Colunm of output grid");
    SetParameterDescription("nooffset", "If 1, then no offset for the first L/C if output grid.");
    SetDefaultParameterInt("nooffset", 0);
    SetMinimumParameterIntValue("nooffset", 0);
    MandatoryOff("nooffset");

    // Parameter Output
    AddParameter(ParameterType_OutputImage, "out", "Output Correlation grid (Vector Image)");
    SetParameterDescription("out","Output Correlation Grid Vector Image (Shift_ran, Shift_azi, Correlation_rate).");
    
    AddRAMParameter();

    SetDocExampleParameterValue("inmaster","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_ML.tiff");
    SetDocExampleParameterValue("inslave","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002_ML.tiff");
    SetDocExampleParameterValue("out", "out_CorrelationGrid.tiff");
  }

  void DoUpdateParameters() override
  {
// Nothing to do here : all parameters are independent
  }

void DoExecute() override
{  
  // Get numeric parameters
  int factorML_azi = GetParameterInt("mlazi");
  int factorML_ran = GetParameterInt("mlran");
  int grid_step_azi = GetParameterInt("gridstepazimut");
  int grid_step_ran = GetParameterInt("gridsteprange");
  int nooffset = GetParameterInt("nooffset");

  // Log information
  otbAppLogINFO(<<"ML Factor on azimut :"<<factorML_azi);
  otbAppLogINFO(<<"ML Factor on range : "<<factorML_ran);
  otbAppLogINFO(<<"Grid Step for range : "<<grid_step_ran);
  otbAppLogINFO(<<"Grid Step for azimut : "<<grid_step_azi);

  // Get master and slave image
  FloatImageType::Pointer MasterPtr = GetParameterFloatImage("inmaster");
  FloatImageType::Pointer SlavePtr = GetParameterFloatImage("inslave");
   
  
  // Correlation Filter
  CorFilterType::Pointer correlationFilter = CorFilterType::New();
  m_Ref.push_back(correlationFilter.GetPointer());
  correlationFilter->SetFixedImage(MasterPtr);
  correlationFilter->SetMovingImage(SlavePtr);
  correlationFilter->Update();

  // Min/Max calculator
  MinMaxCalculatorType::Pointer minMaxFilter = MinMaxCalculatorType::New();
  minMaxFilter->SetImage(correlationFilter->GetOutput());
  minMaxFilter->ComputeMaximum();

  float shiftML_range = ((correlationFilter->GetOutput()->GetLargestPossibleRegion().GetSize()[0]/2.)-
			  minMaxFilter->GetIndexOfMaximum()[0]);
  float shiftML_azimut = ((correlationFilter->GetOutput()->GetLargestPossibleRegion().GetSize()[1]/2.)
  -minMaxFilter->GetIndexOfMaximum()[1]);

  // Correlation Grid Filter 
  CorGridFilterType::Pointer filterCorrelationGrid = CorGridFilterType::New();
  m_Ref.push_back(filterCorrelationGrid.GetPointer());

  // Configure CorrelationGrid Filter
  filterCorrelationGrid->SetMLran(factorML_ran);
  filterCorrelationGrid->SetMLazi(factorML_azi);
  filterCorrelationGrid->SetGridStep(grid_step_ran, grid_step_azi);
  filterCorrelationGrid->SetRoughShift_ran(shiftML_range);
  filterCorrelationGrid->SetRoughShift_azi(shiftML_azimut);
  if (nooffset)
    {
      filterCorrelationGrid->SetFirstLCOffset(false);
    }

  
  // Define the main pipeline 
  filterCorrelationGrid->SetMasterInput(MasterPtr);
  filterCorrelationGrid->SetSlaveInput(SlavePtr);
  SetParameterOutputImage("out", filterCorrelationGrid->GetOutput());
}
   // Vector for filters 
  std::vector<itk::ProcessObject::Pointer> m_Ref;
};


}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARCorrelationGrid)
