/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARStreamingGridInformationImageFilter.h"
#include "otbSARCorrectionGridImageFilter.h"

#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageList.h"
#include "otbImageListToVectorImageFilter.h"
#include "itkAddImageFilter.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{
namespace Wrapper
{

class SARCorrectionGrid : public Application
{
public:
  typedef SARCorrectionGrid Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARCorrectionGrid, otb::Wrapper::Application);

  // Filters
  typedef otb::SARCorrectionGridImageFilter<FloatVectorImageType, FloatVectorImageType> CorrectionGridFilterType;
  typedef otb::SARStreamingGridInformationImageFilter<FloatVectorImageType>             MeanFilterType;
  typedef otb::ImageList<FloatImageType>                                                ImageListType;
  typedef otb::ImageListToVectorImageFilter<ImageListType,
                                       FloatVectorImageType >                           ListConcatenerFilterType;
  typedef otb::MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
                                       FloatImageType::PixelType>                       ExtractROIFilterType;
  typedef itk::AddImageFilter <FloatImageType, FloatImageType, FloatImageType>          AddFilterType;

  typedef ObjectList<ExtractROIFilterType>                                              ExtractROIFilterListType;
  typedef ObjectList<AddFilterType>                                                     AddFilterListType;
  

private:
  void DoInit() override
  {
    SetName("SARCorrectionGrid");
    SetDescription("Computes SAR correction grid.");

    SetDocLongDescription("This application creates a deformation grid by "
      "correcting the DEM grid with correlation grid.\nThe output grid is a "
      "VectorImage composed of two values : shift in range and shift in azimut."
      " The inputs of this application are also vector image with three "
      "components. This application can directly take the projectd DEM.");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "indemgrid",   "Input DEM grid (Vector Image)");
    SetParameterDescription("indemgrid", "Input DEM Grid Vector Image (Shift_ran, Shift_azi, NbDEMPts for contribution).");

    AddParameter(ParameterType_InputImage,  "incorgrid",   "Input Correlation grid (Vector Image)");
    SetParameterDescription("incorgrid", "Input orrelation Grid Vector Image (Shift_ran, Shift_azi, Correlation_rate).");

    AddParameter(ParameterType_OutputImage, "out", "Output Correction grid (Vector Image)");
    SetParameterDescription("out","Output Correction Grid Vector Image (Shift_ran, Shift_azi).");

    AddParameter(ParameterType_Float, "threshold", "Threshold for correlation rate");
    SetParameterDescription("threshold","Threshold for correlation rate.");
    SetDefaultParameterFloat("threshold", 0.3);
    SetMinimumParameterFloatValue("threshold", 0.0001);
    MandatoryOff("threshold");
    
    AddParameter(ParameterType_Float, "gap", "Maximum difference between input grid values and mean values");
    SetParameterDescription("gap","Maximum difference between input grid values and mean values.");
    SetDefaultParameterFloat("gap", 0.7);
    SetMinimumParameterFloatValue("gap", 0.01);
    MandatoryOff("gap");

    AddParameter(ParameterType_String, "advantage", "Give an advantage to DEM or Correlation Grid");
    SetParameterDescription("advantage","Give an advantage to DEM or Correlation Grid.");
    MandatoryOff("advantage");

    AddRAMParameter();

    SetDocExampleParameterValue("indemgrid","./demGrid.tiff");
    SetDocExampleParameterValue("incorgrid","./corGridL.tiff");
    SetDocExampleParameterValue("out","correctionGrid.tif");
  }

  void DoUpdateParameters() override
  {
    // Nothing // TODO: o do here : all parameters are independent
  }

void DoExecute() override
{  
  // Get numeric parameters
  float threshold = GetParameterFloat("threshold");
  float gap = GetParameterFloat("gap");

  // Log information
  otbAppLogINFO(<<"Threshold on correlation rate :"<<threshold);
  otbAppLogINFO(<<"Gap on the difference between grid values and mean values: "<<gap);

  std::string advantage = GetParameterString("advantage");
  bool demGridToFavor = true;
    
  if (advantage == "correlation" || advantage == "cor" || advantage == "corgrid")
    {
      demGridToFavor = false;
    } 
     
  if (demGridToFavor)
    {
      otbAppLogINFO(<<"Opt in favor of projection (DEMGrid)");
    }
  else
    {
      otbAppLogINFO(<<"Opt in favor of correlation (CorGrid)");
    }

  
  // Get input grids
  FloatVectorImageType::Pointer DEMGridPtr = GetParameterFloatVectorImage("indemgrid");
  FloatVectorImageType::Pointer CorGridPtr = GetParameterFloatVectorImage("incorgrid");
  
  if (demGridToFavor)
    {
      // Mean Persistent Filter
      MeanFilterType::Pointer filterMean = MeanFilterType::New();
      m_Ref.push_back(filterMean.GetPointer());

      // Configure Mean Filter
      filterMean->SetThreshold(threshold);
      filterMean->SetInput(DEMGridPtr);
      filterMean->SetEstimateMean(true);

      // Get mean values
      filterMean->Update();
      double meanRange, meanAzimut;
      filterMean->GetMean(meanRange, meanAzimut);

      // Correction Filter
      CorrectionGridFilterType::Pointer filterCorrectionGrid = CorrectionGridFilterType::New();
      m_Ref.push_back(filterCorrectionGrid.GetPointer());
  
      // Configure CorrectionGrid Filter
      filterCorrectionGrid->SetThreshold(threshold);
      filterCorrectionGrid->SetGap(gap);
      filterCorrectionGrid->SetMeanRange(meanRange);
      filterCorrectionGrid->SetMeanAzimut(meanAzimut);
  
      // Define the main pipeline
      filterCorrectionGrid->SetDEMGridInput(DEMGridPtr);
      filterCorrectionGrid->SetCorGridInput(CorGridPtr);
  
      SetParameterOutputImage("out", filterCorrectionGrid->GetOutput());
    }
  else
    {
      // Mean Persistent Filters (for the two grids)
      MeanFilterType::Pointer filterMeanDEM = MeanFilterType::New();
      m_Ref.push_back(filterMeanDEM.GetPointer());

      MeanFilterType::Pointer filterMeanCor = MeanFilterType::New();
      m_Ref.push_back(filterMeanCor.GetPointer());

      // Configure Mean Filters
      filterMeanDEM->SetThreshold(threshold);
      filterMeanDEM->SetInput(DEMGridPtr);
      filterMeanDEM->SetEstimateMean(true);

      filterMeanCor->SetThreshold(threshold);
      filterMeanCor->SetInput(CorGridPtr);
      filterMeanCor->SetEstimateMean(true);

      // Get mean values
      filterMeanDEM->Update();
      double meanRangeDEM, meanAzimutDEM;
      filterMeanDEM->GetMean(meanRangeDEM, meanAzimutDEM);

      filterMeanCor->Update();
      double meanRangeCor, meanAzimutCor;
      filterMeanCor->GetMean(meanRangeCor, meanAzimutCor);

      // Difference between means
      double diffMeanRange = meanRangeCor - meanRangeDEM;
      double diffMeanAzimut = meanAzimutCor - meanAzimutDEM;

      std::cout << "diffMeanRange = " << diffMeanRange << std::endl;
      std::cout << "diffMeanAzimut = " << diffMeanAzimut << std::endl;

      // Correction Filter to replace value into DEMGrid with no occurences (high gap and high threshold) 
      // by mean value 
      CorrectionGridFilterType::Pointer filterCorrectionGrid = CorrectionGridFilterType::New();
      m_Ref.push_back(filterCorrectionGrid.GetPointer());
  
      // Configure CorrectionGrid Filter
      filterCorrectionGrid->SetThreshold(1.5);
      filterCorrectionGrid->SetGap(1000000);
      filterCorrectionGrid->SetMeanRange(meanRangeDEM);
      filterCorrectionGrid->SetMeanAzimut(meanAzimutDEM);
  
      // Define the main pipeline
      filterCorrectionGrid->SetDEMGridInput(DEMGridPtr);
      filterCorrectionGrid->SetCorGridInput(CorGridPtr);

      filterCorrectionGrid->UpdateOutputInformation();
      RegisterPipeline();
      
      // Offset for DEMGrid (Correction with the difference)
      // Instanciate all filters
      ListConcatenerFilterType::Pointer m_Concatener =
	ListConcatenerFilterType::New();
      ExtractROIFilterListType::Pointer m_ExtractorList = 
	ExtractROIFilterListType::New();
      AddFilterListType::Pointer m_AddOffsetList = 
	AddFilterListType::New();
      ImageListType::Pointer m_ImageList =
	ImageListType::New();

      // Check number of components (at least two : range and azimut)
      unsigned int nbComponents = filterCorrectionGrid->GetOutput()->GetNumberOfComponentsPerPixel();
      if (nbComponents < 2)
	{
	  otbAppLogFATAL(<< "Input grid has to contain at least two components");
	}  

      // Extract eah band and apply the corresponding offset
      for (unsigned int i = 0; i < nbComponents; ++i)
	{
	  // Instanciate filters
	  ExtractROIFilterType::Pointer extractor = 
	    ExtractROIFilterType::New();
	  AddFilterType::Pointer addOffset = 
	    AddFilterType::New();

	  // Set the channel to extract
	  extractor->SetInput(filterCorrectionGrid->GetOutput());
	  extractor->SetChannel(i+1);      
	  extractor->UpdateOutputInformation();
	  m_ExtractorList->PushBack(extractor);

	  addOffset->SetInput(extractor->GetOutput());

	  // Adapt offset according to the numnber of band
	  if (i == 0)
	    {
	      addOffset->SetConstant2(diffMeanRange);
	    }
	  else if (i == 1)
	    {
	      addOffset->SetConstant2(diffMeanAzimut);
	    }
	  else
	    {
	      addOffset->SetConstant2(0);
	    }
      
      
	  m_AddOffsetList->PushBack(addOffset);
	  m_ImageList->PushBack(addOffset->GetOutput() );
      
	}
  
      m_Concatener->SetInput( m_ImageList );

      SetParameterOutputImage("out", m_Concatener->GetOutput());
      RegisterPipeline();
      //SetParameterOutputImage("out", DEMGridPtr);

    }
      
}
   // Vector for filters 
  std::vector<itk::ProcessObject::Pointer> m_Ref;
};


}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARCorrectionGrid)
