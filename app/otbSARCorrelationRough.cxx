/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "itkFFTNormalizedCorrelationImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{
namespace Wrapper
{

class SARCorrelationRough : public Application
{
public:
  typedef SARCorrelationRough Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARCorrelationRough, otb::Wrapper::Application);

  // Filters
typedef itk::FFTNormalizedCorrelationImageFilter<FloatImageType, FloatImageType> CorFilterType;
typedef itk::MinimumMaximumImageCalculator< FloatImageType>			    MinMaxCalculatorType;

private:
  void DoInit() override
  {
    SetName("SARCorrelationRough");
    SetDescription("Computes SAR correlation shift (rough shifts).");

    SetDocLongDescription("This application comutes rough shifts between two images : "
    "shift in range and shift in azimut. "
    "The inputs of this application are MultiLooked images (real images).");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "inmaster",   "Input Master image (real image)");
    SetParameterDescription("inmaster", "Master Image (real image).");

    AddParameter(ParameterType_InputImage,  "inslave",   "Input Slave image (real image)");
    SetParameterDescription("inslave", "Slave Image (real image).");

    AddParameter(ParameterType_Int, "mlran", "MultiLook factor on distance");
    SetParameterDescription("mlran","MultiLook factor on distance.");
    SetDefaultParameterInt("mlran", 3);
    SetMinimumParameterIntValue("mlran", 1);
    MandatoryOff("mlran");
    
    AddParameter(ParameterType_Int, "mlazi", "MultiLook factor on azimut");
    SetParameterDescription("mlazi","MultiLook factor on azimut.");
    SetDefaultParameterInt("mlazi", 3);
    SetMinimumParameterIntValue("mlazi", 1);
    MandatoryOff("mlazi");
    
    // Parameter Output
    AddParameter(ParameterType_Float,"shiftranslc","rough shift in range dimension into SLC geometry (output of this application)");
    SetParameterDescription("shiftranslc", "rough shift in range into SLC geometry.");
    SetParameterRole("shiftranslc", Role_Output);
    SetDefaultParameterFloat("shiftranslc", 0);

    AddParameter(ParameterType_Float,"shiftazislc","rough shift in azimut dimension into SLC geometry (output of this application)");
    SetParameterDescription("shiftazislc", "rough shift in azimut into SLC geometry .");
    SetParameterRole("shiftazislc", Role_Output);
    SetDefaultParameterFloat("shiftazislc", 0);

    AddParameter(ParameterType_Float,"shiftranml","rough shift in range dimension into ML geometry (output of this application)");
    SetParameterDescription("shiftranml", "rough shift in range into ML geometry.");
    SetParameterRole("shiftranml", Role_Output);
    SetDefaultParameterFloat("shiftranml", 0);

    AddParameter(ParameterType_Float,"shiftaziml","rough shift in azimut dimension into ML geometry (output of this application)");
    SetParameterDescription("shiftaziml", "rough shift in azimut into ML geometry .");
    SetParameterRole("shiftaziml", Role_Output);
    SetDefaultParameterFloat("shiftaziml", 0);
   
    AddRAMParameter();

    SetDocExampleParameterValue("inmaster","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_ML.tiff");
    SetDocExampleParameterValue("inslave","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002_ML.tiff");
  }

  void DoUpdateParameters() override
  {
// Nothing to do here : all parameters are independent
  }

void DoExecute() override
{  
  // Get numeric parameters
  int factorML_azi = GetParameterInt("mlazi");
  int factorML_ran = GetParameterInt("mlran");

  // Log information
  otbAppLogINFO(<<"ML Factor on azimut :"<<factorML_azi);
  otbAppLogINFO(<<"ML Factor on range : "<<factorML_ran);

  // Get master and slave image
  FloatImageType::Pointer MasterPtr = GetParameterFloatImage("inmaster");
  FloatImageType::Pointer SlavePtr = GetParameterFloatImage("inslave");
   
  
  // Correlation Filter
  CorFilterType::Pointer correlationFilter = CorFilterType::New();
  m_Ref.push_back(correlationFilter.GetPointer());
  correlationFilter->SetFixedImage(MasterPtr);
  correlationFilter->SetMovingImage(SlavePtr);
  correlationFilter->Update();

  // Min/Max calculator
  MinMaxCalculatorType::Pointer minMaxFilter = MinMaxCalculatorType::New();
  minMaxFilter->SetImage(correlationFilter->GetOutput());
  minMaxFilter->ComputeMaximum();

  float shiftSLC_range = ((correlationFilter->GetOutput()->GetLargestPossibleRegion().GetSize()[0]/2.)-
		       minMaxFilter->GetIndexOfMaximum()[0]) * static_cast<float>(factorML_ran);
  float shiftSLC_azimut = ((correlationFilter->GetOutput()->GetLargestPossibleRegion().GetSize()[1]/2.)
			-minMaxFilter->GetIndexOfMaximum()[1]) * static_cast<float>(factorML_azi);

  float shiftML_range = ((correlationFilter->GetOutput()->GetLargestPossibleRegion().GetSize()[0]/2.)-
			  minMaxFilter->GetIndexOfMaximum()[0]);
  float shiftML_azimut = ((correlationFilter->GetOutput()->GetLargestPossibleRegion().GetSize()[1]/2.)
			   -minMaxFilter->GetIndexOfMaximum()[1]);

  // Assigne Outputs
  SetParameterFloat("shiftranslc", shiftSLC_range);
  SetParameterFloat("shiftazislc", shiftSLC_azimut);
  
  SetParameterFloat("shiftranml", shiftML_range);
  SetParameterFloat("shiftaziml", shiftML_azimut);
}
   // Vector for filters 
  std::vector<itk::ProcessObject::Pointer> m_Ref;
};


}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARCorrelationRough)
