/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARStreamingGridHistogramFilter.h"
#include "otbSARUpdateMetadataImageFilter.h"

#include <iostream>
#include <string>
#include <fstream>
#include <complex>
#include <cmath>

// include ossim
#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "ossim/ossimTimeUtilities.h"
#include "ossim/base/ossimKeywordlist.h"
#include "ossim/base/ossimString.h"
#include "ossim/base/ossimDate.h"
#pragma GCC diagnostic pop
#else
#include "ossim/ossimTimeUtilities.h"
#include "ossim/base/ossimKeywordlist.h"
#include "ossim/base/ossimString.h"
#include "ossim/base/ossimDate.h"
#endif

namespace otb
{
namespace Wrapper
{

class SARFineMetadata : public Application
{
public:
  typedef SARFineMetadata Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARFineMetadata, otb::Wrapper::Application);

// Filter
  typedef otb::SARStreamingGridHistogramFilter<FloatVectorImageType> GridHistogramType;
  typedef otb::SARUpdateMetadataImageFilter<ComplexFloatImageType>   UpdateMetadataeFilter;

  // Time/Date ossim 
  typedef ossimplugins::time::ModifiedJulianDate TimeType;
  typedef ossimplugins::time::Duration           DurationType;

private:
  void DoInit() override
  {
    SetName("SARFineMetadata");
    SetDescription("SAR metadata correction.");

    SetDocLongDescription("This application corrects two metadata : the time "
      "of the first line and the slant near range.");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "ingrid",   "Input correlation grid");
    SetParameterDescription("ingrid", "Correlation grid with shifts for the two dimensions and associated correlation rate between two images (for instance : ML Master and simulated amplitude image).");

    AddParameter(ParameterType_InputImage,  "insar",   "Input SAR image");
    SetParameterDescription("insar", "SAR Image to extract initial metadata.");

    AddParameter(ParameterType_OutputFilename, "outfile", "Output file to store new metadata values");
    SetParameterDescription("outfile","Output file to store new metadata values.");
    MandatoryOff("outfile");

    AddParameter(ParameterType_Float,  "stepmax",   "Max step for histogram");
    SetParameterDescription("stepmax", "Max step for histogram.");
    SetDefaultParameterFloat("stepmax", 0.1);
    MandatoryOff("stepmax");
    
    AddParameter(ParameterType_Float,  "threshold",   "Threshold on correlation rate");
    SetParameterDescription("threshold", "Threshold on correlation rate.");
    SetDefaultParameterFloat("threshold", 0.3);
    MandatoryOff("threshold");

    // Output parameters
    AddParameter(ParameterType_String,"timefirstline","Time of the first line (output of this application)");
    SetParameterDescription("timefirstline", "Time of the first line (output of this application).");
    SetParameterRole("timefirstline", Role_Output);
    
    AddParameter(ParameterType_Float,"slantrange","Slant near range (output of this application)");
    SetParameterDescription("slantrange", "Slant near range (output of this application).");
    SetParameterRole("slantrange", Role_Output);

    AddParameter(ParameterType_OutputImage, "out", "Output image with precise metadata");
    SetParameterDescription("out","Output image with precise metadata.");
    
    AddRAMParameter();

    SetDocExampleParameterValue("ingrid","corGrid.tiff");
    SetDocExampleParameterValue("outfile","fine_metadata.txt");
  }

  void DoUpdateParameters() override
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute() override
  {  
    // Get numeric parameters : stepMax and threshold
    float stepMax = GetParameterFloat("stepmax");
    //float Threshold = GetParameterFloat("threshold");

    // Get inputs 
    ComplexFloatImageType::Pointer SARPtr = GetParameterComplexFloatImage("insar");
    FloatVectorImageType::Pointer CorGridPtr = GetParameterFloatVectorImage("ingrid");

    // Get Sar keyWordList
    otb::ImageKeywordlist sar_kwl = SARPtr->GetImageKeywordlist();
  
    // Define C
    const double C = 299792458;

    //////////////////// Slant Range (range dimension) /////////////////////////
    // Initial Parameters
    double stepHistRange = -1.;
    double meanShiftRange = 0.;

    do {
      // Configure Filter
      GridHistogramType::Pointer statisticsRange = GridHistogramType::New();
      m_Ref.push_back(statisticsRange.GetPointer());

      statisticsRange->SetInput(CorGridPtr);
      statisticsRange->SetDimension(0);
      statisticsRange->SetStepHist(stepHistRange);
      statisticsRange->SetThreshold(0.3);
      statisticsRange->SetMeanShift(meanShiftRange);
      statisticsRange->SetNumberHist(256);
  
      // Execute pipeline
      statisticsRange->Update();
      //double step, meanShift;
      statisticsRange->GetGridHistogramInformation(stepHistRange, meanShiftRange);

    } while(stepHistRange > stepMax);

    // New slant range
    double slantRange = atof(sar_kwl.GetMetadataByKey("support_data.slant_range_to_first_pixel").c_str());
    double pixelSpacing_Range = atof(sar_kwl.GetMetadataByKey("support_data.range_spacing").c_str());
    double fineSlantRange = slantRange + ((2*meanShiftRange*pixelSpacing_Range)/C); 


    //////////////////// Time of the first line (azimut dimension) /////////////////////////
    // Initial Parameters
    double stepHistAzimut = -1.;
    double meanShiftAzimut = 0.;

    do {
      // Configure Filter
      GridHistogramType::Pointer statisticsAzimut = GridHistogramType::New();
      m_Ref.push_back(statisticsAzimut.GetPointer());
      
      statisticsAzimut->SetInput(CorGridPtr);
      statisticsAzimut->SetDimension(1);
      statisticsAzimut->SetStepHist(stepHistAzimut);
      statisticsAzimut->SetThreshold(0.3);
      statisticsAzimut->SetMeanShift(meanShiftAzimut);
      statisticsAzimut->SetNumberHist(256);
  
      // Execute pipeline
      statisticsAzimut->Update();
      //double step, meanShift;
      statisticsAzimut->GetGridHistogramInformation(stepHistAzimut, meanShiftAzimut);

    } while(stepHistAzimut > stepMax);

    // New time for the first line
    std::string timeFirstLine = sar_kwl.GetMetadataByKey("support_data.first_line_time");
    double prf = atof(sar_kwl.GetMetadataByKey("support_data.pulse_repetition_frequency").c_str());
    // Change into julian day 
    ossimplugins::time::ModifiedJulianDate time_first_pixel_Date = ossimplugins::time::toModifiedJulianDate(timeFirstLine);
    ossimplugins::time::ModifiedJulianDate fine_Time_first_pixel_Date = time_first_pixel_Date + ossimplugins::time::seconds(meanShiftAzimut/prf);
    // Change into std::string
    std::string fineTimeFirstLine = to_simple_string(fine_Time_first_pixel_Date);
    
    
    //////////////////// Output File to store fine metadata /////////////////////////
    // if outfile parameter, fill the file with new metadata values
    std::string outFile = GetParameterString("outfile");

    if (!outFile.empty())
      {
	std::ofstream MetadataFile(outFile, std::ios::app);
	    
	if (MetadataFile)
	  {
	    MetadataFile << std::setprecision(11);
	    MetadataFile << "PRECISE TIME OF FIRST LINE : " << fineTimeFirstLine << std::endl;
	    MetadataFile << "PRECISE SLANT RANGE : " << fineSlantRange << std::endl;
	    MetadataFile.close();
	  }
      }

    // Assigne timefirstline and slantrange
    SetParameterString("timefirstline", fineTimeFirstLine);

    SetParameterFloat("slantrange", fineSlantRange);

    // Copie input image and update the metadata into a naw geom file
    UpdateMetadataeFilter::Pointer updateMetadataFilter = UpdateMetadataeFilter::New();
    m_Ref.push_back(updateMetadataFilter.GetPointer());
    updateMetadataFilter->SetTimeFirstLine(fineTimeFirstLine);
    updateMetadataFilter->SetSlantRange(fineSlantRange);
    updateMetadataFilter->SetInput(SARPtr);

    SetParameterOutputImage("out", updateMetadataFilter->GetOutput());
  }

  // Vector for filters 
  std::vector<itk::ProcessObject::Pointer> m_Ref;

};

}
  
}
OTB_APPLICATION_EXPORT(otb::Wrapper::SARFineMetadata)
