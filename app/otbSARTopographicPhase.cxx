/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARStreamingGridInformationImageFilter.h"
#include "otbSARTopographicPhaseImageFilter.h"

#include "otbWrapperOutputImageParameter.h"
#include "otbWrapperTypes.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{

  namespace Wrapper
  {

    class SARTopographicPhase : public Application
    {
    public:
      typedef SARTopographicPhase Self;
      typedef itk::SmartPointer<Self> Pointer; 

      itkNewMacro(Self);
      itkTypeMacro(SARTopographicPhase, otb::Wrapper::Application);
     
      // Filters
      typedef otb::SARStreamingGridInformationImageFilter<FloatVectorImageType> GridInformationFilterType;
      typedef otb::SARTopographicPhaseImageFilter<FloatVectorImageType, FloatVectorImageType> TopographicPhaseFilterType;

    private:
      void DoInit() override
      {
	SetName("SARTopographicPhase");
	SetDescription("Estimates topographic phase for interferogram.");

	SetDocLongDescription("This application estimates the topographic phase"
        " between two SAR images in order to improve the interferogram.");

	//Optional descriptors
	SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
	SetDocAuthors("OTB-Team");
	SetDocSeeAlso(" ");
	AddDocTag(Tags::SAR);
	AddDocTag("DiapOTB");

	//Parameter declarations
	AddParameter(ParameterType_InputImage,  "ingrid",   "Input deformation grid");
	SetParameterDescription("ingrid", "Input deformation grid.");

	AddParameter(ParameterType_InputImage,  "insarslave",   "Input SAR Slave image (metadata)");
	SetParameterDescription("insarslave", "Input SAR Slave image (metadata).");

	AddParameter(ParameterType_InputImage,  "incartmeanmaster",   "Input Cartesian Mean Master image");
	SetParameterDescription("incartmeanmaster", "Input Cartesian Mean Master image.");

	AddParameter(ParameterType_Int,  "gridsteprange", "Grid Step for range dimension into SLC geometry");
	SetParameterDescription("gridsteprange", "Grid Step for range dimension into  SLC geometry.");
	SetDefaultParameterInt("gridsteprange", 150);
	MandatoryOff("gridsteprange");

	AddParameter(ParameterType_Int,  "gridstepazimut", "Grid Step for azimut dimension into SLC geometry");
	SetParameterDescription("gridstepazimut", "Grid Step for azimut dimension into  SLC geometry.");
	SetDefaultParameterInt("gridstepazimut", 150);
	MandatoryOff("gridstepazimut");

	AddParameter(ParameterType_Int,  "mlran", "ML factor on range");
	SetParameterDescription("mlran", "ML factor on range.");
	SetDefaultParameterInt("mlran", 3);
	MandatoryOff("mlran");

	AddParameter(ParameterType_Int,  "mlazi", "ML factor on azimut");
	SetParameterDescription("mlazi", "ML factor on azimut.");
	SetDefaultParameterInt("mlazi", 3);
	MandatoryOff("mlazi");

	AddParameter(ParameterType_Bool, "cartcopy", "Copy cartesian coordonates into output");
	SetParameterDescription("cartcopy", "If true, Copy cartesian coordonates into output.");
	

	AddParameter(ParameterType_OutputImage, "out", "Topographic Phase (And Copy of Cartesian Mean Master Image)");
	SetParameterDescription("out","Output Image : Topographic Phase (And Copy of Cartesian Mean Master Image).");

	AddRAMParameter();

	SetDocExampleParameterValue("insarslave","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002.tiff");
	SetDocExampleParameterValue("incartmeanmaster","CartesianMaster.tiff");
	SetDocExampleParameterValue("ingrid","./FineDeformation.tiff");
	SetDocExampleParameterValue("out","topographicPhase.tiff");
      }

      void DoUpdateParameters() override
      {
	// Nothing to do here : all parameters are independent
      }

      void DoExecute() override
      { 
	// Get numeric parameters : gridStep and ML factors
	int gridStepRan = GetParameterInt("gridsteprange");
	int gridStepAzi = GetParameterInt("gridstepazimut");
	int mlRan = GetParameterInt("mlran");
	int mlAzi = GetParameterInt("mlazi");
	    
	otbAppLogINFO(<<"Grid Step into Range : "<<gridStepRan);
	otbAppLogINFO(<<"Grid Step into Azimut : "<<gridStepAzi);
	otbAppLogINFO(<<"ML factor on range : "<<mlRan);
	otbAppLogINFO(<<"ML factor on azimut : "<<mlAzi);
    
	// Retrive input grid
	FloatVectorImageType::Pointer GridPtr = GetParameterFloatVectorImage("ingrid");

	///////// Persistent filter to get some information about grid (min/max shift inot each dimension ) /////////
	typedef otb::SARStreamingGridInformationImageFilter<FloatVectorImageType> GridInformationFilterType;
	GridInformationFilterType::Pointer filterGridInfo = GridInformationFilterType::New();
	m_Ref.push_back(filterGridInfo.GetPointer());
	filterGridInfo->SetInput(GridPtr);
	filterGridInfo->SetEstimateMean(false);

	// Retrieve min/max into intput grid for each dimension
	filterGridInfo->Update();
	double minRan, minAzi, maxRan, maxAzi; 
	filterGridInfo->GetMinMax(minRan, minAzi, maxRan, maxAzi);
	double maxAbsRan = std::max(std::abs(minRan), std::abs(maxRan));
	double maxAbsAzi = std::max(std::abs(minAzi), std::abs(maxAzi)); 

	///////////////////////////////////// Topographic Phase Filter /////////////////////////////////////////////// 
	// Instanciate the Coregistration filter
	TopographicPhaseFilterType::Pointer filterTopoPhase = TopographicPhaseFilterType::New();
	m_Ref.push_back(filterTopoPhase.GetPointer());
	filterTopoPhase->SetMLRan(mlRan);
	filterTopoPhase->SetMLAzi(mlAzi);
	filterTopoPhase->SetGridStep(gridStepRan, gridStepAzi);
	filterTopoPhase->SetMaxShiftInRange(maxAbsRan);
	filterTopoPhase->SetMaxShiftInAzimut(maxAbsAzi);

	if (IsParameterEnabled("cartcopy"))
	  {
	    filterTopoPhase->SetMasterCopy(true);
	  }

	ComplexFloatImageType::Pointer SlaveSAR = GetParameterComplexFloatImage("insarslave");
	filterTopoPhase->SetSlaveImageKeyWorList(SlaveSAR->GetImageKeywordlist());

	// Pipeline
	filterTopoPhase->SetMasterCartesianMeanInput(GetParameterFloatVectorImage("incartmeanmaster"));
	filterTopoPhase->SetGridInput(GridPtr);

	// Output
	SetParameterOutputImage("out", filterTopoPhase->GetOutput());
      }
      // Vector for filters 
      std::vector<itk::ProcessObject::Pointer> m_Ref;

    };

  }

}


 
OTB_APPLICATION_EXPORT(otb::Wrapper::SARTopographicPhase)
