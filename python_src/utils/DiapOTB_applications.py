#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""
    DiapOTB applications
    ====================
 
    Use the module to DiapOTB and OTB applications from Python
    Applications are used with ExecuteAndWriteOutput as execute command => I/O on disk and not integrated 
    for a mode In-Memory. 
     
    Applications list :
    -------------------

    extractROI,
    burstExtraction,
    doppler0,
    multilook,
    demProjection,
    cartesianMeanEstimation,
    fineGridDeformation,
    coRegistration,
    deramp,
    interferogram,
    esd,
    concatenate,
    orthorectification,
    GridOffset.
"""

import os
import sys

from utils import func_utils as fUtils
import otbApplication as otb

##### Define function for OTB applications used into this processing #####
def extractROI(inImg, outPath, ram="4000") :
    """
        Launch ExtractROI application (from OTB).
 
        This application extracts a ROI from an input image. 
        This particular case is just conversion from a single band CInt16 to a dual band FLOAT32 (much faster than OTB_DynamicConvert)
 
        :param inImg: Input SLC Image.
        :param outPath: Output burst.
    
        :type inImg: string
        :type outPath: string

        :return: None 
    """    
    app = otb.Registry.CreateApplication("ExtractROI")
    app.SetParameterString("ram", ram)
    app.SetParameterString("mode","fit")
    app.SetParameterString("mode.fit.im", inImg)
    app.SetParameterString("in", inImg)
    app.SetParameterString("out", outPath)
    app.ExecuteAndWriteOutput()


def burstExtraction(inImg, burstIndex, outPath, allpixels="true", ram="4000"):
    """
        Launch SARBurstExtraction application (from OTB).
 
        This application performs a burst extraction by keeping only lines and samples of a required burst.
        Two modes are available for the output image : with all pixels and with only valid pixels.
 
        :param inImg: Input Sentinel1 IW SLC Image.
        :param burstIndex: Index for the required Burst.
        :param allpixels: Select the modes for output image.
        :param outPath: Output burst.
    
        :type inImg: string
        :type burstIndex: int
        :type allpixels: bool
        :type outPath: string

        :return: None 
    """
    appBurstExtraction = otb.Registry.CreateApplication("SARBurstExtraction")
    appBurstExtraction.SetParameterString("in", inImg)
    appBurstExtraction.SetParameterString("out", outPath)
    appBurstExtraction.SetParameterInt("burstindex", burstIndex)
    appBurstExtraction.SetParameterString("allpixels", allpixels)
    appBurstExtraction.SetParameterString("ram", ram)
    appBurstExtraction.ExecuteAndWriteOutput()

def doppler0(insar, outPath, ram="4000"):
    """
        Launch SARDoppler0 application (from DiapOTB).
    
        This application estimates the value of Doppler 0 for a SAR image. The algorithm is based on the CDE 
        method described into [Estimating the Doppler centroid of SAR data].
 
        :param insar: Input SAR image.
        :param outPath: Output file to store Doppler 0 value..
    
        :type insar: string
        :type outPath: string

        :return: Doppler0 value (azmiut dimension)
        :rtype: float 
    """
    appDoppler0 = otb.Registry.CreateApplication("SARDoppler0")
    appDoppler0.SetParameterString("insar", insar)
    appDoppler0.SetParameterString("outfile", outPath)
    appDoppler0.SetParameterString("ram", "4000")
    appDoppler0.ExecuteAndWriteOutput()

    return appDoppler0.GetParameterFloat('doppler0')

def multilook(inImg, mlran, mlazi, mlgain, outPath, ram="4000"):
    """
        Launch SARMultiLook application (from DiapOTB).
    
        This application creates the multi-look image of a SLC product.
 
        :param inImg: Input image (complex here).
        :param mlran: Averaging on distance).
        :param mlazi: Averaging on azimut.
        :param mlgain: Gain to apply on ML image.
        :param outPath: Output ML Image.
    
        :type inImg: string
        :type mlran: int
        :type mlazi: int
        :type mlgain: float
        :type outPath: string

        :return: None 
    """
    appMultiLook = otb.Registry.CreateApplication("SARMultiLook")
    appMultiLook.SetParameterString("incomplex", inImg)
    appMultiLook.SetParameterString("out", outPath)
    appMultiLook.SetParameterInt("mlran",mlran)
    appMultiLook.SetParameterInt("mlazi",mlazi)
    appMultiLook.SetParameterFloat("mlgain", mlgain)
    appMultiLook.SetParameterString("ram", ram)
    appMultiLook.ExecuteAndWriteOutput()


def demToAmplitude(insar, dem, mlran, mlazi, mlgain, outPath, nodata=-32768, ram="4000"):
    """
        Launch SARDEMToAmplitude application (from DiapOTB).
   
        This application executes the Simu_SAR step with three internal applications : 
        SARDEMProjection, SARAmplitudeEstimation and SARMultiLook.
 
        :param insar: SAR Image to extract SAR geometry.
        :param indem: DEM to extract DEM geometry.
        :param mlran: Averaging on distance).
        :param mlazi: Averaging on azimut.
        :param mlgain: Gain to apply on ML image.
        :param nodata: No Data values for the DEM.
        :param outPath: Output Amplitude Image.
    
        :type insar: string
        :type indem: string
        :type mlran: int
        :type mlazi: int
        :type mlgain: float
        :type nodata: int
        :type outPath: string

        :return: None 
    """
    appDEMToAmplitude = otb.Registry.CreateApplication("SARDEMToAmplitude")
    appDEMToAmplitude.SetParameterString("insar", insar)
    appDEMToAmplitude.SetParameterString("indem", dem)
    appDEMToAmplitude.SetParameterString("out", outPath)
    appDEMToAmplitude.SetParameterInt("mlran", mlran)
    appDEMToAmplitude.SetParameterInt("mlazi", mlazi)
    appDEMToAmplitude.SetParameterFloat("mlgain", mlgain)
    appDEMToAmplitude.SetParameterInt("nodata", nodata)
    appDEMToAmplitude.SetParameterString("ram", ram)
    appDEMToAmplitude.ExecuteAndWriteOutput()


def demProjection(insar, indem, withxyz, outPath, nodata=-32768, ram="4000"):
    """
        Launch SARDEMProjection application (from DiapOTB).
    
        This application puts a DEM file into SAR geometry and estimates two additional coordinates.
        In all for each point of the DEM input four components are calculated : 
        C (colunm into SAR image), L (line into SAR image), Z and Y.
 
        :param insar: SAR Image to extract SAR geometry.
        :param indem: DEM to perform projection on.
        :param withxyz: Set XYZ Cartesian components into projection.
        :param nodata: No Data values for the DEM.
        :param outPath: Output Vector Image with at least four components (C,L,Z,Y).
    
        :type insar: string
        :type indem: string
        :type withxyz: bool
        :type nodata: int
        :type outPath: string

        :return: Range/Azimut direction for DEM scan and Gain value
        :rtype: int, int, float 
    """
    appDEMProjection = otb.Registry.CreateApplication("SARDEMProjection")
    appDEMProjection.SetParameterString("insar", insar)
    appDEMProjection.SetParameterString("indem", indem)
    appDEMProjection.SetParameterString("withxyz", withxyz)
    appDEMProjection.SetParameterInt("nodata", nodata)
    appDEMProjection.SetParameterString("out", outPath)
    appDEMProjection.SetParameterString("ram", ram)
    appDEMProjection.ExecuteAndWriteOutput()

    return appDEMProjection.GetParameterInt('directiontoscandemc'), appDEMProjection.GetParameterInt('directiontoscandeml'), appDEMProjection.GetParameterFloat('gain')

def cartesianMeanEstimation(insar,indem, indemproj, indirectiondemc, indirectiondeml, outPath, ram="4000"):
    """
        Launch SARCartesianMeanEstimation application (from DiapOTB).
    
        This application estimates a simulated cartesian mean image thanks to a DEM file.
 
        :param insar: SAR Image to extract SAR geometry.
        :param indem: DEM to extract DEM geometry.
        :param indemproj: Input vector image for cartesian mean estimation.
        :param indirectiondemc: Range direction for DEM scan
        :param indirectiondeml: Azimut direction for DEM scan
        :param outPath: Output cartesian (mean) Image for DEM Projection.
    
        :type insar: string
        :type indem: string
        :type indemproj: string
        :type indirectiondemc: int
        :type indirectiondeml: int
        :type outPath: string

        :return: None 
    """
    appCartMean = otb.Registry.CreateApplication("SARCartesianMeanEstimation")
    appCartMean.SetParameterString("insar", insar)
    appCartMean.SetParameterString("indem", indem)
    appCartMean.SetParameterString("indemproj",  indemproj)
    appCartMean.SetParameterInt("indirectiondemc", indirectiondemc)
    appCartMean.SetParameterInt("indirectiondeml", indirectiondeml)
    appCartMean.SetParameterInt("mlran", 1)
    appCartMean.SetParameterInt("mlazi", 1)
    appCartMean.SetParameterString("ram", ram)
    appCartMean.SetParameterString("out",  outPath)
    appCartMean.ExecuteAndWriteOutput()


def correlationGrid(inmaster, inslave, mlran, mlazi, gridsteprange, gridstepazimut, outPath, ram="4000"):
    """
        Launch SARCorrelationGrid application (from DiapOTB).
 
        This application computes correlation shifts between two images : 
        shift in range and shift in azimut. 
        The inputs of this application are MultiLooked images (real images)
 
        :param inmaster: Master Image (real image).
        :param inslave: Slave Image (real image).
        :param mlran: MultiLook factor on distance.
        :param mlazi: MultiLook factor on azimut.
        :param gridsteprange: Grid step for range dimension (into SLC/SAR geometry).
        :param gridstepazimut: Grid step for azimut dimension (into SLC/SAR geometry).
        :param outPath: Output Correlation Grid Vector Image (Shift_ran, Shift_azi).
    
        :type inmaster: string
        :type inslave: string
        :type mlran: int
        :type mlazi: int
        :type gridsteprange: int
        :type gridstepazimut: int
        :type outPath: string

        :return: None
    """
    appCorGrid = otb.Registry.CreateApplication("SARCorrelationGrid")
    appCorGrid.SetParameterString("inmaster", inmaster)
    appCorGrid.SetParameterString("inslave", inslave)
    appCorGrid.SetParameterInt("mlran", mlran)
    appCorGrid.SetParameterInt("mlazi", mlazi)
    appCorGrid.SetParameterInt("gridsteprange", gridsteprange)
    appCorGrid.SetParameterInt("gridstepazimut", gridstepazimut)
    appCorGrid.SetParameterString("out", outPath)
    appCorGrid.SetParameterString("ram", ram)
    appCorGrid.ExecuteAndWriteOutput()


def fineMetadata(insar, ingrid, stepmax, threshold, outPath, ram="4000"):
    """
        Launch SARFineMetadata application (from DiapOTB).
   
        This application corrects two metadata : 
        the time of the first line and the slant near range.
 
        :param insar: SAR Image to extract SAR geometry.
        :param ingrid: Input correlation grid.
        :param stepmax: Max step for histogram.
        :param threshold: Threshold on correlation rate.
        :param outPath: Output file to store new metadata values.
    
        :type insar: string
        :type ingrid: string
        :type stepmax: float
        :type threshold: float
        :type outPath: string

        :return: None 
    """
    appFineMetadata = otb.Registry.CreateApplication("SARFineMetadata")
    appFineMetadata.SetParameterString("insar", insar)
    appFineMetadata.SetParameterString("ingrid", ingrid)
    appFineMetadata.SetParameterFloat("stepmax", stepmax)
    appFineMetadata.SetParameterFloat("threshold", threshold)
    appFineMetadata.SetParameterString("outfile", outPath)
    appFineMetadata.ExecuteAndWriteOutput()


def fineGridDeformation(indem, insarmaster, insarslave, inmlmaster, inmlslave, indemprojmaster, indemprojslave, mlran, mlazi, gridsteprange, gridstepazimut, threshold, gap, advantage, outPath, ram="4000"):
    """
        Launch SARFineDeformationGrid application (from DiapOTB).
 
        This application executes the geo_grid step with 
        three internal applications : SARCorrelationGrid, SARDEMGrid and 
        SARCorrectionGrid. The aim is to obtain a fine deformation grid between
        master and slave SAR images
 
        :param indem: DEM to extract DEM geometry.
        :param insarmaster: SAR Master Image to extract SAR geometry.
        :param insarslave: SAR Slave Image to extract SAR geometry.
        :param inmlmaster: Master Image  Multilooked (real image).
        :param inmlslave: Slave Image Multilooked (real image).
        :param indemprojmaster: Input vector of DEM projected into SAR Master geometry.
        :param indemprojslave: Input vector of DEM projected into SAR Slave geometry.
        :param mlran: MultiLook factor on distance.
        :param mlazi: MultiLook factor on azimut.
        :param gridsteprange: Grid step for range dimension (into SLC/SAR geometry).
        :param gridstepazimut: Grid step for azimut dimension (into SLC/SAR geometry).
        :param threshold: Threshold for correlation rate.
        :param gap: Maximum difference between input grid values and mean values.
        :param advantage: Give an advantage to DEM or Correlation Grid.
        :param outPath: Output Deformation Grid Vector Image (Shift_ran, Shift_azi).
    
        :type indem: string 
        :type insarmaster: string
        :type insarslave: string
        :type inmlmaster: string
        :type inmlslave: string
        :type indemprojmaster: string
        :type indemprojslave: string
        :type mlran: int
        :type mlazi: int
        :type gridsteprange: int
        :type gridstepazimut: int
        :type threshold: float
        :type gap: int
        :type advantage: string
        :type outPath: string

        :return: None
    """
    appFineDeformationGrid = otb.Registry.CreateApplication("SARFineDeformationGrid")
    appFineDeformationGrid.SetParameterString("indem", indem)
    appFineDeformationGrid.SetParameterString("insarmaster", insarmaster)
    appFineDeformationGrid.SetParameterString("insarslave", insarslave)
    appFineDeformationGrid.SetParameterString("inmlmaster", inmlmaster)
    appFineDeformationGrid.SetParameterString("inmlslave", inmlslave)
    appFineDeformationGrid.SetParameterString("indemprojmaster", indemprojmaster)
    appFineDeformationGrid.SetParameterString("indemprojslave", indemprojslave)
    appFineDeformationGrid.SetParameterInt("mlran", mlran)
    appFineDeformationGrid.SetParameterInt("mlazi", mlazi)
    appFineDeformationGrid.SetParameterInt("gridsteprange", gridsteprange)
    appFineDeformationGrid.SetParameterInt("gridstepazimut", gridstepazimut)
    appFineDeformationGrid.SetParameterFloat("threshold", threshold)
    appFineDeformationGrid.SetParameterFloat("gap", gap)
    appFineDeformationGrid.SetParameterString("advantage", advantage)
    appFineDeformationGrid.SetParameterString("out", outPath)
    appFineDeformationGrid.SetParameterString("ram", ram)
    appFineDeformationGrid.ExecuteAndWriteOutput()


def coRegistration(insarmaster, insarslave, ingrid, gridsteprange, gridstepazimut, doppler0, nbramps, outPath, ram="4000"):
    """
        Launch SARCoRegistration application (from DiapOTB).

        This application does the coregistration between
        two SAR images thanks to a deformation grid.

        :param insarmaster: SAR Master Image to extract SAR geometry.
        :param insarslave: SAR Slave Image to extract SAR geometry.
        :param ingrid: Input deformation grid.
        :param gridsteprange: Grid step for range dimension (into SLC/SAR geometry).
        :param gridstepazimut: Grid step for azimut dimension (into SLC/SAR geometry).
        :param doppler0: Doppler 0 in azimut for SAR Slave Image.
        :param nbramps: Number of ramps for coregistration
        :param outPath: Coregistrated slave image into master geometry.
    
        :type insarmaster: string
        :type insarslave: string
        :type ingrid: string
        :type gridsteprange: int
        :type gridstepazimut: int
        :type doppler0: int
        :type nbramps: int
        :type outPath: string

        :return: None
    """    
    appCoRegistration = otb.Registry.CreateApplication("SARCoRegistration")
    appCoRegistration.SetParameterString("insarmaster", insarmaster)
    appCoRegistration.SetParameterString("insarslave", insarslave)
    appCoRegistration.SetParameterString("ingrid", ingrid)
    appCoRegistration.SetParameterInt("gridsteprange", gridsteprange)
    appCoRegistration.SetParameterInt("gridstepazimut", gridstepazimut)
    appCoRegistration.SetParameterFloat("doppler0", doppler0)
    appCoRegistration.SetParameterInt("sizetiles", 50)
    appCoRegistration.SetParameterInt("margin", 7)
    appCoRegistration.SetParameterInt("nbramps", nbramps)
    appCoRegistration.SetParameterString("ram", ram)
    appCoRegistration.SetParameterString("out", outPath)
    appCoRegistration.ExecuteAndWriteOutput()

def deramp(inImg, reramp, shift, ingrid, inslave, gridsteprange, gridstepazimut, outPath, ram="4000"):
    """
        Launch SARDeramp application (from DiapOTB).
 
        This application does the deramping or reramping of S1 Iw burst.
 
        :param inImg: Input burst (from S1 Iw product).    
        :param reramp: Activate Reramping mode
        :param shift: Activate Shift mode (To reramp/deramp accordingly a deformation grid)
        :param ingrid: Input deformation grid.
        :param insarslave: SAR Slave Image to extract SAR geometry.    
        :param gridsteprange: Grid step for range dimension (into SLC/SAR geometry).
        :param gridstepazimut: Grid step for azimut dimension (into SLC/SAR geometry).
        :param outPath: Coregistrated slave image into master geometry.
    
        :type inImg: string
        :type reramp: bool
        :type shift: bool 
        :type ingrid: string
        :type inslave: string
        :type gridsteprange: int
        :type gridstepazimut: int
        :type outPath: string

        :return: None
    """
    appDerampSlave = otb.Registry.CreateApplication("SARDeramp")
    appDerampSlave.SetParameterString("in", inImg)
    appDerampSlave.SetParameterString("out", outPath)
    if fUtils.str2bool(reramp) :
        appDerampSlave.SetParameterString("reramp", reramp)
    if fUtils.str2bool(shift) :
        appDerampSlave.SetParameterString("shift", shift)
        appDerampSlave.SetParameterString("ingrid", ingrid)
        appDerampSlave.SetParameterInt("gridsteprange", gridsteprange)
        appDerampSlave.SetParameterInt("gridstepazimut", gridstepazimut)
        appDerampSlave.SetParameterString("inslave", inslave)
    appDerampSlave.SetParameterString("ram", ram)
    appDerampSlave.ExecuteAndWriteOutput()

def interferogram(insarmaster, incoregistratedslave, insarslave, ingrid, incartmeanmaster, gridsteprange, gridstepazimut, mlran, mlazi, gain, outPath, ram="4000"):
    """
        Launch SARRobustInterferogram application (from DiapOTB).
    
        This application estimates robust interferogram with flatenning and averaging.
 
        :param insarmaster: Input SAR Master image.
        :param incoregistratedslave: Input SAR Slave image (Coregistrated image).
        :param insarslave: Input SAR Slave image (Metadata).    
        :param ingrid: Input deformation grid.
        :param incartmeanmaster: Input Cartesian Mean Master image.
        :param gridsteprange: Grid step for range dimension (into SLC/SAR geometry).
        :param gridstepazimut: Grid step for azimut dimension (into SLC/SAR geometry).
        :param gain: Gain to apply for amplitude estimation.
        :param outPath: Output interferogram (ML geometry).
    
        :type insarmaster: string
        :type incoregistratedslave: string
        :type insarslave: string
        :type ingrid: string
        :type incartmeanmaster: string
        :type gridsteprange: int
        :type gridstepazimut: int
        :type gain: float
        :type outPath: string

        :return: None 
    """
    appInterferogram = otb.Registry.CreateApplication("SARRobustInterferogram")
    appInterferogram.SetParameterString("insarmaster", insarmaster)
    appInterferogram.SetParameterString("incoregistratedslave", incoregistratedslave)
    appInterferogram.SetParameterString("insarslave", insarslave)
    appInterferogram.SetParameterString("ingrid",  ingrid)
    appInterferogram.SetParameterString("incartmeanmaster", incartmeanmaster)
    appInterferogram.SetParameterInt("mlran", mlran)
    appInterferogram.SetParameterInt("mlazi", mlazi)
    appInterferogram.SetParameterInt("marginran", 1)
    appInterferogram.SetParameterInt("marginazi", 1)
    appInterferogram.SetParameterInt("gridsteprange", gridsteprange)
    appInterferogram.SetParameterInt("gridstepazimut", gridstepazimut)
    appInterferogram.SetParameterFloat("gain", gain)
    appInterferogram.SetParameterString("ram", ram)
    appInterferogram.SetParameterString("out", outPath)
    appInterferogram.ExecuteAndWriteOutput()

def esd(ininterfup, ininterflow, insar, burstIndex, outPath, ram="4000"):
    """
        Launch SARESD application (from DiapOTB).
    
        This application applies esd processing on two consecutives bursts dimension.
 
        :param ininterfup: First interferogram.
        :param ininterflow: Second interferogram.
        :param insar: SAR Image to extract SAR geometry.    
        :param burstIndex: Index of first interferogram.
        :param outPath: Output Tmp.
    
        :type ininterfup: string
        :type ininterflow: string
        :type insar: string
        :type burstIndex: int
        :type outPath: string

        :return: Azimut shift to correct phase jump.
        :rtype: float 
    """
    appESD = otb.Registry.CreateApplication("SARESD")
    appESD.SetParameterString("ininterfup", ininterfup)
    appESD.SetParameterString("ininterflow", ininterflow)
    appESD.SetParameterString("insar", insar)
    appESD.SetParameterInt("burstindex", burstIndex)
    appESD.SetParameterFloat("threshold", 0.3)
    appESD.SetParameterInt("mlazi", 1)
    appESD.SetParameterString("ram", ram)
    appESD.SetParameterString("out", outPath)
    appESD.Execute()

    return appESD.GetParameterFloat('azishift')

def GridOffset(ingrid, offsetazi, outPath, ram="4000"):
    """
        Launch SARGridOffset application (from DiapOTB).
    
        This application applies offsets on a deformation grid for each dimension.
 
        :param ingrid: Input Grid Vector Image (Shift_ran, Shift_azi).    
        :param offsetazi: Offset on azimut.
        :param outPath: Output Grid (Tmp).
    
        :type ingrid: string
        :type offsetazi: int
        :type outPath: string

       :return: None
    """
    appGridOff = otb.Registry.CreateApplication("SARGridOffset")
    appGridOff.SetParameterString("ingrid", ingrid)
    appGridOff.SetParameterFloat("offsetran", 0.)
    appGridOff.SetParameterFloat("offsetazi", offsetazi)
    appGridOff.SetParameterString("ram", ram)
    appGridOff.SetParameterString("out", outPath)
    appGridOff.ExecuteAndWriteOutput()

def concatenate(list_of_Interferograms, insar, firstBurst, outPath, ram="4000"):
    """
        Launch SARConcatenateBursts application (from DiapOTB).

        This application performs a burst concatenation and provides a SAR Deburst Image. 
        It reads the input image list (single bursts) and generates a whole SAR image with deburst operations.
    
        :param list_of_Interferograms: The list of bursts to concatenate.     
        :param insar: Raw Sentinel1 IW SLC image, or any extract of such made by OTB (geom file needed).
        :param firstBurst: Index for the first required Burst. 
        :param outPath: The concatenated and debursted output image.
    
        :type list_of_Interferograms: list
        :type insar: string
        :type firstBurst: int
        :type outPath: string

        :return: None
    """
    appCon = otb.Registry.CreateApplication("SARConcatenateBursts")
    appCon.SetParameterStringList("il", list_of_Interferograms)
    appCon.SetParameterString("insar", insar)
    appCon.SetParameterInt("burstindex", firstBurst)
    appCon.SetParameterString("out", outPath)
    appCon.SetParameterString("ram", ram)
    appCon.ExecuteAndWriteOutput()

def orthorectification(inImg, spacingxy, hgts_path, geoid_path, outPath, ram="4000"):
    """
        Launch OrthoRectification application (from OTB).

        This application allows ortho-rectifying optical and radar images

        :param inImg: Input Image (SAR geometry).     
        :param spacingxy: Pixel size for ouput image (spacingxy in x and -spacingxy in y).
        :param hgts_path: Path to hgt (SRTM) files.
        :param geoid_path: Path to geoid.
        :param outPath: Output image (ortho geometry).
    
        :type inImg: string
        :type spacingxy: float
        :type hgts_path: string
        :type geoid_path: string
        :type outPath: string

        :return: None
    """
    OrthoRectification = otb.Registry.CreateApplication("OrthoRectification")
    OrthoRectification.SetParameterString("io.in", inImg)
    OrthoRectification.SetParameterString("opt.ram", ram)
    OrthoRectification.SetParameterString("map", "epsg")
    OrthoRectification.SetParameterString("outputs.spacingx", spacingxy)
    OrthoRectification.SetParameterString("outputs.spacingy", str(-abs(float(spacingxy))))
    OrthoRectification.SetParameterString("opt.gridspacing", str(abs(float(spacingxy)*10)))
    OrthoRectification.SetParameterString("outputs.mode", 'autosize')    
    OrthoRectification.SetParameterString("elev.dem", hgts_path)
    OrthoRectification.SetParameterString("elev.geoid", geoid_path)   
    OrthoRectification.SetParameterString("io.out", outPath)
    OrthoRectification.ExecuteAndWriteOutput()
