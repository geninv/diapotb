#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

""" 
    diapOTB.py
    ==========

    Interferometry chain between two SAR images (master/slave) for S1 Strimap and Cosmo products
 
"""

__author__ = "POC-THALES"
__version__ = "0.1"
__status__ = "Developpement"
__date__ = "27/10/2017"
__last_modified__ = "05/02/2020"

# Imports
import sys
import logging
import os
import argparse
import h5py

from processings import Pre_Processing
from processings import Ground
from processings import DInSar
from processings import Metadata_Correction

from utils import func_utils

# Main
if __name__ == "__main__":
    
    # Check arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("configfile", help="input conguration file for the application DiapOTB")
    args = parser.parse_args()
    print(args.configfile)

    func_utils.init_logger()
       
    dataConfig = func_utils.load_configfile(args.configfile)

    
    # Get dictionaries
    dict_Global = dataConfig['Global']
    dict_PreProcessing = dataConfig['Pre_Processing']
    dict_Metadata_Correction = dataConfig['Metadata_Correction']
    dict_DInSAR = dataConfig['DIn_SAR']

    # Get elements from configuration file
    # Global
    master_Image = dict_Global['in']['Master_Image_Path']
    slave_Image =  dict_Global['in']['Slave_Image_Path']
    dem =  dict_Global['in']['DEM_Path']
    output_dir = dict_Global['out']['output_dir']

    satellite = "default"
    mode = "default"

    if 'sensor' in dict_Global:
        satellite = dict_Global['sensor']['satellite']
        mode = dict_Global['sensor']['mode']

    # Pre_Processing
    ml_range = dict_PreProcessing['parameter']['ML_range'] 
    ml_azimut = dict_PreProcessing['parameter']['ML_azimut']
    ml_gain = dict_PreProcessing['parameter']['ML_gain']
    dop_file = dict_PreProcessing['out']['doppler_file']

    # Metadata_Correction
    activateMetadataCorrection = dict_Metadata_Correction['parameter']['activate']
    ml_simu_range = ml_range
    ml_simu_azimut = ml_azimut
    ml_simu_gain = 1.
    ml_correlSimu_range = ml_range
    ml_correlSimu_azimut = ml_azimut
    correlSimu_gridstep_range = dict_Metadata_Correction['parameter']['GridStep_range']
    correlSimu_gridstep_azimut = dict_Metadata_Correction['parameter']['GridStep_azimut']
    fine_metadata_file = dict_Metadata_Correction['out']['fine_metadata_file']

    # DIn_SAR
    geoGrid_gridstep_range = dict_DInSAR['parameter']['GridStep_range']
    geoGrid_gridstep_azimut = dict_DInSAR['parameter']['GridStep_azimut']
    geoGrid_threshold = dict_DInSAR['parameter']['Grid_Threshold']
    geoGrid_gap = dict_DInSAR['parameter']['Grid_Gap']
    ml_geoGrid_range = ml_range
    ml_geoGrid_azimut = ml_azimut
    gain_interfero = dict_DInSAR['parameter']['Interferogram_gain']
    
    if (geoGrid_threshold < 0) or (geoGrid_threshold > 1) :
        func_utils.log(logging.CRITICAL, "Error, Wrong Threshold for fine deformation grid")

    # Check if images exist
    if not os.path.exists(master_Image) :
        func_utils.log(logging.CRITICAL, "Error, {img} does not exist. Check its path.".format(img=master_Image))
        quit()
    if not os.path.exists(slave_Image) :
        func_utils.log(logging.CRITICAL, "Error, {img} does not exist. Check its path.".format(img=slave_Image))
        quit()
    if not os.path.exists(dem) :
        func_utils.log(logging.CRITICAL, "Error, {img} does not exist. Check its path.".format(img=dem))
        quit()
    if not os.path.exists(output_dir):
        print("The output directory does not exist and will be created")
        os.makedirs(output_dir)
    else :
        print("The output directory exists. Some files can be overwritten")


    # Init file handler (all normaly print on std is redirected into info.log) 
    # To use previous print on std, use printOnStd
    func_utils.init_fileLog(output_dir) 

    # Recap of input parameter into info.log
    func_utils.log(logging.INFO, "########### Input Parameters for the current execution ############## ")
    func_utils.log(logging.INFO, " Pre_Processing : ")
    func_utils.log(logging.INFO, "ml_range : {param}".format(param=ml_range))
    func_utils.log(logging.INFO, "ml_azimut : {param}".format(param=ml_azimut))
    func_utils.log(logging.INFO, "ml_gain : {param}".format(param=ml_gain))
    func_utils.log(logging.INFO, "dop_file : {param}".format(param=dop_file))

    # Metadata_Correction
    func_utils.log(logging.INFO, " Metadata_Correction : ")
    func_utils.log(logging.INFO, "activateMetadataCorrection : {param}".format(param=activateMetadataCorrection))
    if activateMetadataCorrection :
        func_utils.log(logging.INFO, "ml_simu_range : {param}".format(param=ml_simu_range))
        func_utils.log(logging.INFO, "ml_simu_azimut : {param}".format(param=ml_simu_azimut))
        func_utils.log(logging.INFO, "ml_simu_gain : {param}".format(param=ml_simu_gain))
        func_utils.log(logging.INFO, "ml_correlSimu_range : {param}".format(param=ml_correlSimu_range))
        func_utils.log(logging.INFO, "ml_correlSimu_azimut : {param}".format(param=ml_correlSimu_azimut))
        func_utils.log(logging.INFO, "correlSimu_gridstep_range : {param}".format(param=correlSimu_gridstep_range))
        func_utils.log(logging.INFO, "correlSimu_gridstep_azimut : {param}".format(param=correlSimu_gridstep_azimut))
        func_utils.log(logging.INFO, "fine_metadata_file : {param}".format(param=fine_metadata_file))

    # DIn_SAR
    func_utils.log(logging.INFO, " DIn_SAR : ")
    func_utils.log(logging.INFO, "geoGrid_gridstep_range : {param}".format(param=geoGrid_gridstep_range))
    func_utils.log(logging.INFO, "geoGrid_gridstep_azimut : {param}".format(param=geoGrid_gridstep_azimut))
    func_utils.log(logging.INFO, "geoGrid_threshold : {param}".format(param=geoGrid_threshold))
    func_utils.log(logging.INFO, "geoGrid_gap : {param}".format(param=geoGrid_gap))
    func_utils.log(logging.INFO, "ml_geoGrid_range : {param}".format(param=ml_geoGrid_range))
    func_utils.log(logging.INFO, "ml_geoGrid_azimut : {param}".format(param=ml_geoGrid_azimut))
    func_utils.log(logging.INFO, "gain_interfero : {param}".format(param=gain_interfero))
    

    func_utils.log(logging.INFO, "########### Input Images for the current execution ############## ")
        
    master_Image_base = os.path.basename(master_Image)
    slave_Image_base = os.path.basename(slave_Image)

    # Check extension (if .h5 => HDF5 file => Cosmo Sensor)
    master_ext = master_Image.split(".")[-1:]
    slave_ext = slave_Image.split(".")[-1:]

    func_utils.log(logging.INFO, "master_ext = {ext}".format(ext=master_ext[0]))
    func_utils.log(logging.INFO, "slave_ext = {ext}".format(ext=slave_ext[0]))

    
    if master_ext[0] == "h5" :
        master_H5 = h5py.File(master_Image, 'r')
        lDataSet_master = list(master_H5.keys())
       

        if len(lDataSet_master) != 1 :
            func_utils.log(logging.CRITICAL, "Error, H5 input files does not contain the expected dataset")
            quit()

        if lDataSet_master[0] != "S01" :
            func_utils.log(logging.CRITICAL, "Error, H5 input files does not contain the expected dataset")
            quit()

        master_S01 = dict(master_H5['S01'])
        
        if not 'SBI' in master_S01:
            func_utils.log(logging.CRITICAL, "Error, H5 input files does not contain the expected dataset")
            quit()

        # Change the name of master and slave image to read directly the //S01/SBI
        master_Image = "HDF5:" + master_Image + "://S01/SBI"
        # Adapt sattelite
        satellite = "cosmo"


    if slave_ext[0] == "h5" :      
        slave_H5 = h5py.File(slave_Image, 'r')
        lDataSet_slave = list(slave_H5.keys())

        if len(lDataSet_slave) != 1 :
            func_utils.log(logging.CRITICAL, "Error, H5 input files does not contain the expected dataset")
            quit()

        if lDataSet_slave[0] != "S01" :
            func_utils.log(logging.CRITICAL, "Error, H5 input files does not contain the expected dataset")
            quit()

        slave_S01 = dict(slave_H5['S01'])
        
        if not 'SBI' in slave_S01 :
            func_utils.log(logging.CRITICAL, "Error, H5 input files does not contain the expected dataset")
            quit()
        
        slave_Image = "HDF5:" + slave_Image + "://S01/SBI"

    func_utils.log(logging.INFO, "master_Image = {img}".format(img=master_Image))
    func_utils.log(logging.INFO, "slave_Image = {img}".format(img=slave_Image))
    func_utils.log(logging.INFO, "dem : {param}".format(param=dem))


    func_utils.printOnStd("\n Beginning of DiapOTB processing (S1 SM or Cosmo mode) \n")
    func_utils.log(logging.INFO, "############ Beginning of DiapOTB processing (S1 SM or Cosmo mode) ##############")

    ####################### Pre Processing Chain ##########################
    # Master
    func_utils.printOnStd("\n Master Pre_Processing chain \n")
    func_utils.log(logging.INFO, "Master Pre_Processing Application")
 
    paramPreMaster = {}
    paramPreMaster['ml_range'] = ml_range
    paramPreMaster['ml_azimut'] = ml_azimut
    paramPreMaster['ml_gain'] = ml_gain
    paramPreMaster['dop_file'] = dop_file

    dop0Master = Pre_Processing.extractToMultilook(master_Image, master_Image_base, paramPreMaster, "Others",
                                                   output_dir)
    # Slave
    func_utils.printOnStd("\n Slave Pre_Processing chain \n")
    func_utils.log(logging.INFO, "Slave Pre_Processing Application")

    paramPreSlave = {}
    paramPreSlave['ml_range'] = ml_range
    paramPreSlave['ml_azimut'] = ml_azimut
    paramPreSlave['ml_gain'] = ml_gain
    paramPreSlave['dop_file'] = dop_file

    dop0Slave = Pre_Processing.extractToMultilook(slave_Image, slave_Image_base, paramPreSlave, "Others",
                                                  output_dir)



    # ######################## Metadata Correction Chain #############################
    if activateMetadataCorrection :       
        # TO DO
        func_utils.printOnStd("Metadata Correction Chain not available for now")

        # paramMetadata = {}
        # paramMetadata['ml_range'] = ml_simu_range
        # paramMetadata['ml_azimut'] = ml_simu_azimut
        # paramMetadata['ml_gain'] = ml_simu_gain
        # paramMetadata['geoGrid_gridstep_range'] = correlSimu_gridstep_range
        # paramMetadata['geoGrid_gridstep_azimut'] = correlSimu_gridstep_azimut
        # paramMetadata['nodata'] = -32768
        # paramMetadata['fine_metadata_file'] = fine_metadata_file

        # Metadata_Correction.fineMetadata(master_Image, master_Image_base, dem, paramMetadata, output_dir)

       
    ######################### Ground Chain #############################
    # Master
    func_utils.printOnStd("\n Master Ground chain \n")
    func_utils.log(logging.INFO, "Master Ground Application")
 
    paramGroundMaster = {}
    paramGroundMaster['nodata'] = -32768
    paramGroundMaster['withxyz'] = "true"
    paramGroundMaster['for_slave_Image'] = False
    
    Ground.demProjectionAndCartesianEstimation(master_Image, master_Image_base, dem, paramGroundMaster, 
                                               "Others", output_dir)
    

    # Slave
    func_utils.printOnStd("\n Slave Ground chain \n")
    func_utils.log(logging.INFO, "Slave Ground Application")

    paramGroundSlave = {}
    paramGroundSlave['nodata'] = -32768
    paramGroundSlave['withxyz'] = "true"
    paramGroundSlave['for_slave_Image'] = True
    
    Ground.demProjectionAndCartesianEstimation(slave_Image, slave_Image_base, dem, paramGroundSlave, 
                                               "Others", output_dir)
  

    ######################## DIn_SAR Chain #############################
    func_utils.printOnStd("\n DIn_SAR chain \n")
    func_utils.log(logging.INFO, "DIn_SAR chain")

    advantage = "projection"
    if satellite == "cosmo" or satellite == "CSK" :
        advantage = "correlation"

    # Create param
    param = {}
    param['ml_azimut'] = ml_azimut
    param['ml_range'] = ml_range
    param['ml_geoGrid_azimut'] = ml_geoGrid_azimut
    param['ml_geoGrid_range'] = ml_geoGrid_range
    param['geoGrid_gridstep_range'] = geoGrid_gridstep_range
    param['geoGrid_gridstep_azimut'] = geoGrid_gridstep_azimut
    param['geoGrid_threshold'] = geoGrid_threshold
    param['geoGrid_gap'] = geoGrid_gap
    param['doppler0'] = dop0Slave
    param['gain_interfero'] = gain_interfero
    param['advantage'] = advantage

    DInSar.gridToInterferogram(dem, master_Image, master_Image_base, slave_Image, slave_Image_base, output_dir, output_dir, param, "Others", output_dir)

    func_utils.printOnStd("\n End of DiapOTB processing (S1 SM or Cosmo mode) \n")
    func_utils.log(logging.INFO, "############ End of DiapOTB processing  (S1 SM or Cosmo mode) ##############")
