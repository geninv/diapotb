#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""
    The ``Ground`` chain
    ============================
 
    Use the module to launch Ground chain.
 
    main function : demProjectionAndCartesianEstimation
    ----------------------------------------------------
  
"""

import os
import sys
from functools import partial

import utils.DiapOTB_applications as diapOTBApp



def demProjectionAndCartesianEstimation(sar_Image, sar_Image_base, dem, param, mode, output_dir):
    """
        Main function for Ground chain
    
        This function applies several processing according to the selected mode :
        For S1IW mode : For each burst, Burst Extraction + Deramping + Doppler0 estimation + MultiLook processing
        For other mode : Doppler0 estimation + MultiLook processing
    
        :param sar_Image: SAR image name (with path included) 
        :param sar_Image_base: SAR image name
        :param dem: Input DEM
        :param param: Dictionary to retrieve all parameters used into this processing chain 
        :param mode: Selected mode according to input product : S1IW or other (for Cosmo and S1 StripMap products) 
        :param output_dir: Output directory 

        :type sar_Image: string 
        :type sar_Image_base: string
        :type dem: string
        :type param: dict
        :type mode: string
        :type output_dir: string
    """
    if mode == 'S1_IW' :
        return demProjectionAndCartesianEstimation_S1IW(sar_Image, sar_Image_base, dem, param, output_dir)
    else :
        return demProjectionAndCartesianEstimation_Others(sar_Image, sar_Image_base, dem, param, output_dir)
    

def demProjectionAndCartesianEstimation_Others(sar_Image, sar_Image_base, dem, param, output_dir):
    """
        Main function for Ground chain and other than S1IW mode (S1SM and Cosmo)
    
        This function applies several processing DEM Projection + Cartesian estimation
    
        :param sar_Image: SAR image name (with path included) 
        :param sar_Image_base: SAR image name   
        :param dem: Input DEM
        :param param: Dictionary to retrieve all parameters used into this processing chain 
        :param output_dir: Output directory 

        :type sar_Image: string 
        :type sar_Image_base: string
        :type dem: string
        :type param: dict
        :type output_dir: string
    """    
    
    for_slave_Image = False

    # Get elements into param dictionnaries 
    nodata = param['nodata']
    withxyz = param['withxyz']
    for_slave_Image = param['for_slave_Image']
    

    ## SARDEMProjection Application ##
    print("\n SARDEMProjection Application \n")
    extProj = "_Master.tif"
    if for_slave_Image :
        extProj =  "_Slave.tif"

    demProj = "demProj"  +extProj

    directiontoscandemc, directiontoscandeml, gain = diapOTBApp.demProjection(insar=sar_Image, indem=dem, withxyz=withxyz, outPath=os.path.join(output_dir, demProj), nodata=nodata)


    if not for_slave_Image :
        ## SARCartesianMeanEstimation Application ##
        print("\n SARCartesianMeanEstimation Application \n")
        master_cartesian_mean = "CartMeanMaster.tif"

        diapOTBApp.cartesianMeanEstimation(insar=sar_Image, indem=dem, 
                                indemproj=os.path.join(output_dir, demProj), 
                                indirectiondemc=directiontoscandemc, indirectiondeml=directiontoscandeml, 
                                outPath=os.path.join(output_dir, master_cartesian_mean))


def demProjectionAndCartesianEstimation_S1IW(sar_Image, sar_Image_base, dem, param, output_dir):
    """
        Main function for Ground chain and S1IW mode
    
        This function applies several processing DEM Projection + Cartesian estimation
    
        :param sar_Image: SAR image name (with path included) 
        :param sar_Image_base: SAR image name   
        :param dem: Input DEM
        :param param: Dictionary to retrieve all parameters used into this processing chain 
        :param output_dir: Output directory 

        :type sar_Image: string 
        :type sar_Image_base: string
        :type dem: string
        :type param: dict
        :type output_dir: string
    """    
    
    for_slave_Image = False

    # Get elements into param dictionnaries 
    nodata = param['nodata']
    withxyz = param['withxyz']
    validBurstMaster = param['validBurstMaster']
    validBurstSlave = []
    if "validBurstSlave" in param :
        validBurstSlave = param['validBurstSlave']
        for_slave_Image = True

    # Loop on burst
    for id_loop in range(0, len(validBurstMaster)):
        
        burstId_dir = validBurstMaster[id_loop]
        
        burstId = burstId_dir
        if for_slave_Image :
            burstId = validBurstSlave[id_loop]

        print("\n BurstId = " + str(burstId_dir) + "\n")

        burst_dir = os.path.join(output_dir, "burst" + str(burstId_dir))

        burstDeramp = os.path.splitext(sar_Image_base)[0] + "_burst" + str(burstId) + "_deramp" + ".tif"
        
    
        ## SARDEMProjection Application ##
        print("\n SARDEMProjection Application \n")
        extProj = "_Master.tif"
        if for_slave_Image :
            extProj =  "_Slave.tif"

        demProj = "demProj" +  "_burst" + str(burstId) +extProj
        
        directiontoscandemc, directiontoscandeml, gain = diapOTBApp.demProjection(insar=os.path.join(burst_dir, burstDeramp), indem=dem, withxyz=withxyz, outPath=os.path.join(burst_dir, demProj), nodata=nodata)
        
      
        if not for_slave_Image :
            ## SARCartesianMeanEstimation Application ##
            print("\n SARCartesianMeanEstimation Application \n")
            master_cartesian_mean = "CartMeanMaster" +  "_burst" + str(burstId) + ".tif"

            diapOTBApp.cartesianMeanEstimation(insar=os.path.join(burst_dir, burstDeramp), indem=dem, 
                                    indemproj=os.path.join(burst_dir, demProj), 
                                    indirectiondemc=directiontoscandemc, indirectiondeml=directiontoscandeml, 
                                    outPath=os.path.join(burst_dir, master_cartesian_mean))
