#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""
    The ``Metadata Correction`` chain
    =================================
 
    Use the module to launch Metadata Correction chain (only available for S1 SM or Cosmo).
 
    main function : metadataCorrection
    ----------------------------------    
 
"""

import os
import sys
from functools import partial

import utils.DiapOTB_applications as diapOTBApp



def fineMetadata(sar_Image, sar_Image_base, dem, param, output_dir):
    """
        Main function for Metadata Correction chain
    
        This function applies several processing (only available for S1 SM and Cosmo products) :
        Dem to Amplitude application + Correlation grid + Metadata Correction
    
        :param sar_Image: SAR image name (with path included) 
        :param sar_Image_base: SAR image name
        :param dem: Input DEM
        :param param: Dictionary to retrieve all parameters used into this processing chain 
        :param output_dir: Output directory 

        :type sar_Image: string 
        :type sar_Image_base: string
        :type dem: string
        :type param: dict
        :type output_dir: string
    """
   
    # Get elements into param dictionnaries 
    ml_azimut = param['ml_azimut']
    ml_range = param['ml_range']
    ml_gain = param['ml_gain']
    geoGrid_gridstep_range = param['geoGrid_gridstep_range']
    geoGrid_gridstep_azimut = param['geoGrid_gridstep_azimut']   
    nodata = param['nodata']
    fine_metadata_file = param['fine_metadata_file']
    
    ## SARDEMToAmplitude Application ##
    amplitude_simu_image = os.path.splitext(sar_Image_base)[0] + "_simuSAR" + "_ml" + str(ml_azimut) + str(ml_range) + ".tif"
    diapOTBApp.demToAmplitude(insar=sar_Image, dem=dem, mlran=ml_range, mlazi=ml_azimut, mlgain=ml_gain, 
                              outPath=os.path.join(output_dir, amplitude_simu_image), nodata=nodata)


    ## SARCorrelationGrid Application ##
    correl_grid = "correl_simu" + "_gridstep" + str(geoGrid_gridstep_azimut) + str(geoGrid_gridstep_range) + ".tif"
    sar_Image_ML = os.path.splitext(sar_Image_base)[0] + "_ml" + str(ml_azimut) + str(ml_range) + ".tif"

    diapOTBApp.correlationGrid(inmaster=os.path.join(output_dir, sar_Image_ML), 
                               inslave=os.path.join(output_dir, amplitude_simu_image), 
                               mlran=ml_range, mlazi=ml_azimut, gridsteprange=geoGrid_gridstep_range, 
                               gridstepazimut=geoGrid_gridstep_azimut, 
                               outPath=os.path.join(output_dir, correl_grid))

    ## SARFineMetadata Application ##
    diapOTBApp.fineMetadata(insar=sar_Image, ingrid=os.path.join(output_dir, correl_grid), 
                            stepmax=0.1, threshold=0.3, outPath=os.path.join(output_dir, fine_metadata_file))


