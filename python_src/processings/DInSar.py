#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""
    The ``DINSAR`` chain
    ======================
 
    Use the module to launch DInSar processigns.
 
    main function : gridToInterferogram
    -----------------------------------
 
"""

import os
import sys
from functools import partial

import utils.DiapOTB_applications as diapOTBApp
from utils import func_utils

import otbApplication as otb


# Coorection for interferograms thanks to esd shifts
def esd_correct_interferogram(validBurstMaster, validBurstSlave, master_Image_base, slave_Image_base, gridsteprange, gridstepazimut, gain, output_dir, master_dir, doppler0, iter_esd, list_of_Grids, azimut_shift_esd, azimut_shift_esd_global) : 
    """
        Correct interferograms with esd shifts.
    
        This function is used inside the ESD loop (called by gridToInterferogram_S1IW).
    
        :param validBurstMaster: Master burst to process 
        :param validBurstSlave: Slave burst to process 
        :param master_Image_base: Master image name   
        :param slave_Image_base: Slave image name
        :param gridsteprange: Grid step for range dimension (into SLC/SAR geometry).
        :param gridstepazimut: Grid step for azimut dimension (into SLC/SAR geometry).
        :param gain: Gain to apply for amplitude estimation.
        :param output_dir: Output directory 
        :param master_dir: Output directory (to store result on master image only)
        :param doppler0: Doppler0 values for slave bursts 
        :param iter_esd: Current iteration number
        :param list_of_Grids: List of deformation grid (one per burst)
        :param azimut_shift_esd: List of azimut shift found by SARESD Application (one per burst)
        :param azimut_shift_esd_global: List of azimut shift added for each iteration (one per burst)

        :type validBurstMaster: list
        :type validBurstSlave: list
        :type master_Image_base: string
        :type slave_Image_base: string
        :type gridsteprange: int
        :type gridstepazimut: int
        :type gain: float
        :type output_dir: string 
        :type master_dir: string 
        :type doppler0: list
        :type iter_esd: int 
        :type list_of_Grids: list 
        :type azimut_shift_esd: list
        :type azimut_shift_esd_global: list

    """
    firstBurst = validBurstMaster[0]
    lastBurst = validBurstMaster[len(validBurstMaster)-1]

    list_of_Interferograms = []

    for id_loop in range(0, len(validBurstMaster)):
        burstId = validBurstMaster[id_loop]
        burstId_slave = validBurstSlave[id_loop]

        # Paths
        Master_burst_dir = os.path.join(master_dir, "burst" + str(burstId))
        burst_dir = os.path.join(output_dir, "burst" + str(burstId))
        esd_dir = os.path.join(burst_dir, "esd")
        gridOffset_path = os.path.join(esd_dir, "gridOffOut" + "_burst" + str(burstId) + "_iter" + str(0) + ".tif")

        burstM = os.path.splitext(master_Image_base)[0] + "_burst" + str(burstId) + ".tif"
        burstS = os.path.splitext(slave_Image_base)[0] + "_burst" + str(burstId_slave) + ".tif"
        burstDerampM = os.path.splitext(master_Image_base)[0] + "_burst" + str(burstId) + "_deramp" + ".tif"
        burstDerampS = os.path.splitext(slave_Image_base)[0] + "_burst" + str(burstId_slave) + "_deramp" + ".tif"
        master_cartesian_mean = "CartMeanMaster" +  "_burst" + str(burstId) + ".tif"

        # Adjust azimut shift according to the burstId
        aziShift = 0.
        if burstId == (lastBurst - 1) :
            # Only accumulation between iterations
            azimut_shift_esd_global[burstId-firstBurst] += azimut_shift_esd[burstId-firstBurst]
            aziShift = azimut_shift_esd_global[burstId-firstBurst]
        elif burstId == lastBurst :
            # Same as the lastBurst -1 
            aziShift = azimut_shift_esd_global[burstId - 1 - firstBurst]
        else : 
            # Accumulation of means between the current burstId and the next index 
            azimut_shift_esd_global[burstId-firstBurst] += ((azimut_shift_esd[burstId-firstBurst] + 
                                                             azimut_shift_esd[burstId-firstBurst+1])/2)
            aziShift = azimut_shift_esd_global[burstId-firstBurst]

        # Offset of deformation grid with the azimut shift
        ## SARGridOffset Application ##
        print("\n GridOffset Application \n")
        appGridOff = otb.Registry.CreateApplication("SARGridOffset")
        appGridOff.SetParameterString("ingrid", list_of_Grids[burstId-firstBurst])
        appGridOff.SetParameterFloat("offsetran", 0.)
        appGridOff.SetParameterFloat("offsetazi", aziShift)
        appGridOff.SetParameterString("ram", "4000")
        appGridOff.SetParameterString("out", gridOffset_path)
        appGridOff.Execute()



        ######## Step 3 : SARCoRegistration + SARDeramp + SARRobustInterferogram #######
        ## SARCoRegistration Application (changeo step) ##
        print("\n SARCoRegistration Application \n")
        slave_Image_CoRe = os.path.splitext(slave_Image_base)[0] + "_burst" + str(burstId) + "_coregistrated.tif"
        coRe_path = os.path.join(esd_dir, slave_Image_CoRe)
        appCoRegistration = otb.Registry.CreateApplication("SARCoRegistration")
        appCoRegistration.SetParameterString("insarmaster", os.path.join(Master_burst_dir, burstDerampM))
        appCoRegistration.SetParameterString("insarslave", os.path.join(burst_dir, burstDerampS))
        appCoRegistration.SetParameterInputImage("ingrid", appGridOff.GetParameterOutputImage("out"))
        appCoRegistration.SetParameterInt("gridstepazimut", gridstepazimut)
        appCoRegistration.SetParameterInt("gridsteprange", gridsteprange)
        appCoRegistration.SetParameterFloat("doppler0", doppler0[burstId-validBurstMaster[0]])
        appCoRegistration.SetParameterInt("sizetiles", 50)
        appCoRegistration.SetParameterInt("margin", 7)
        appCoRegistration.SetParameterInt("nbramps", 5121) #256*2*10+1
        appCoRegistration.SetParameterString("ram", "4000")
        appCoRegistration.SetParameterString("out", coRe_path)
        appCoRegistration.Execute()


        ######## SARDeramp Application (reramp mode) #######
        print("\n Deramping Application \n")
        # Slave (CoRegistrated)
        burstCoReRerampS = os.path.splitext(slave_Image_base)[0] + "_burst" + str(burstId) + "_coregistrated_reramp" + ".tif"
        deramp_Path = os.path.join(esd_dir, burstCoReRerampS)

        appDerampSlave = otb.Registry.CreateApplication("SARDeramp")
        appDerampSlave.SetParameterInputImage("in", appCoRegistration.GetParameterOutputImage("out"))
        appDerampSlave.SetParameterString("out", deramp_Path)
        appDerampSlave.SetParameterString("reramp", "true")
        appDerampSlave.SetParameterString("shift", "true")
        appDerampSlave.SetParameterInputImage("ingrid", appGridOff.GetParameterOutputImage("out"))
        appDerampSlave.SetParameterString("inslave", os.path.join(burst_dir, burstDerampS))
        appDerampSlave.SetParameterInt("gridsteprange", gridsteprange)
        appDerampSlave.SetParameterInt("gridstepazimut", gridstepazimut)
        appDerampSlave.SetParameterString("ram", "4000")
        appDerampSlave.Execute()


        ## SARRobustInterferogram Application (interf step) ##
        print("\n SARRobustInterferogram Application \n")
        interferogram_path = os.path.join(esd_dir, "interferogram" + "_burst" + str(burstId) + "_iter" + str(iter_esd) + ".tif")

        appInterferogram = otb.Registry.CreateApplication("SARRobustInterferogram")
        appInterferogram.SetParameterString("insarmaster", os.path.join(Master_burst_dir, burstM))
        appInterferogram.SetParameterInputImage("incoregistratedslave", appDerampSlave.GetParameterOutputImage("out"))
        appInterferogram.SetParameterString("insarslave", os.path.join(burst_dir, burstS))
        appInterferogram.SetParameterInputImage("ingrid",  appGridOff.GetParameterOutputImage("out"))
        appInterferogram.SetParameterString("incartmeanmaster", os.path.join(Master_burst_dir, master_cartesian_mean))
        appInterferogram.SetParameterInt("mlran", 1)
        appInterferogram.SetParameterInt("mlazi", 1)
        appInterferogram.SetParameterInt("marginran", 1)
        appInterferogram.SetParameterInt("marginazi", 1)
        appInterferogram.SetParameterInt("gridsteprange", gridsteprange)
        appInterferogram.SetParameterInt("gridstepazimut", gridstepazimut)
        appInterferogram.SetParameterFloat("gain", gain)
        appInterferogram.SetParameterString("ram", "4000")
        appInterferogram.SetParameterString("out", interferogram_path)
        appInterferogram.ExecuteAndWriteOutput()

        list_of_Interferograms.append(interferogram_path)

    return list_of_Interferograms


def gridToInterferogram(dem, master_Image, master_Image_base, slave_Image, slave_Image_base, master_dir, slave_dir, param, mode, output_dir):
    """
        Main function for DInSAR processing
    
        This function applies several processing to provide the result interferogram
    
        :param dem: Input DEM  
        :param master_Image: Master image name (with path included) 
        :param master_Image_base: Master image name   
        :param slave_Image: Slave image name (with path included) 
        :param slave_Image_base: Slave image name
        :param master_dir: Output directory (to store result on master image only)
        :param slave_dir: Output directory (to store result on slave image only)
        :param param: Dictionary to retrieve all parameters used into this processing chain 
        :param mode: Selected mode according to input product : S1IW or other (for Cosmo and S1 StripMap products) 
        :param output_dir: Output directory 

        :type dem: string 
        :type master_Image: string 
        :type master_Image_base: string
        :type slave_Image: string
        :type slave_Image_base: string
        :type master_dir: string
        :type slave_dir: string
        :type param: dict
        :type mode: string
        :type output_dir: string

        :return: list of grids and interferograms
        :rtype: list, list 
    """
    if mode == 'S1_IW' :
        return gridToInterferogram_S1IW(dem, master_Image, master_Image_base, slave_Image, slave_Image_base, master_dir, slave_dir, param, output_dir)
    else :
        return gridToInterferogram_Others(dem, master_Image, master_Image_base, slave_Image, slave_Image_base, master_dir, slave_dir, param, output_dir)

    

def gridToInterferogram_Others(dem, master_Image, master_Image_base, slave_Image, slave_Image_base, master_dir, slave_dir, param, output_dir):
    """
        Main function for DInSAR processing and other than S1IW mode (S1SM and Cosmo)
    
        This function applies several processing to provide the result interferogram 
    
        :param dem: Input DEM  
        :param master_Image: Master image name (with path included) 
        :param master_Image_base: Master image name   
        :param slave_Image: Slave image name (with path included) 
        :param slave_Image_base: Slave image name
        :param master_dir: Output directory (to store result on master image only)
        :param slave_dir: Output directory (to store result on slave image only)
        :param param: Dictionary to retrieve all parameters used into this processing chain 
        :param output_dir: Output directory 

        :type dem: string 
        :type master_Image: string 
        :type master_Image_base: string
        :type slave_Image: string
        :type slave_Image_base: string
        :type master_dir: string
        :type slave_dir: string
        :type param: dict
        :type output_dir: string

        :return: list of grids and interferograms
        :rtype: list, list 
    """    
    # Get elements into param dictionnaries 
    ml_azimut = param['ml_azimut']
    ml_range = param['ml_range']
    ml_geoGrid_azimut = param['ml_geoGrid_azimut']
    ml_geoGrid_range = param['ml_geoGrid_range']
    geoGrid_gridstep_range = param['geoGrid_gridstep_range']
    geoGrid_gridstep_azimut = param['geoGrid_gridstep_azimut']    
    geoGrid_threshold = param['geoGrid_threshold']
    geoGrid_gap = param['geoGrid_gap']
    doppler0 = param['doppler0']
    gain_interfero = param['gain_interfero']
    advantage = param['advantage']

    with_interferogram = True

    if "with_interferogram" in param :
        with_interferogram = func_utils.str2bool(param['with_interferogram'])
    
    print("doppler0 = " + str(doppler0))

    nbramps = 257

    # define applications functions with the previous parameters
    fineGridDeformationApp = partial(diapOTBApp.fineGridDeformation, indem=dem, mlran=ml_geoGrid_range, 
                                     mlazi=ml_geoGrid_azimut, gridsteprange=geoGrid_gridstep_range, 
                                     gridstepazimut=geoGrid_gridstep_azimut, 
                                     threshold=geoGrid_threshold, gap=geoGrid_gap, advantage=advantage)
    
    coRegistrationApp = partial(diapOTBApp.coRegistration, gridsteprange=geoGrid_gridstep_range, gridstepazimut=geoGrid_gridstep_azimut, nbramps=nbramps)

    derampApp = partial(diapOTBApp.deramp, gridsteprange=geoGrid_gridstep_range, gridstepazimut=geoGrid_gridstep_azimut, reramp="true", shift="true")

    interferogramApp = partial(diapOTBApp.interferogram, gridsteprange=geoGrid_gridstep_range, gridstepazimut=geoGrid_gridstep_azimut, mlran=ml_range, mlazi=ml_azimut, gain=gain_interfero)
        

    # Empty list
    list_of_Grids = []
    list_of_Interferograms = []

  
    master_Image_ML = os.path.splitext(master_Image_base)[0] + "_ml" + str(ml_azimut) + str(ml_range) + ".tif"
    slave_Image_ML = os.path.splitext(slave_Image_base)[0] + "_ml" + str(ml_azimut) + str(ml_range) + ".tif"


    demProj_Master = "demProj_Master.tif"
    demProj_Slave = "demProj_Slave.tif"
    master_cartesian_mean = "CartMeanMaster.tif"

    ## SARFineDeformationGrid Application ##
    print("\n SARFineDeformationGrid Application \n")
    fine_grid = "fineDeformationGrid.tif"
    fine_gird_path = os.path.join(output_dir, fine_grid)
    fineGridDeformationApp(insarmaster=master_Image, 
                           insarslave=slave_Image,
                           inmlmaster=os.path.join(master_dir, master_Image_ML), 
                           inmlslave=os.path.join(output_dir, slave_Image_ML), 
                           indemprojmaster=os.path.join(master_dir, demProj_Master), 
                           indemprojslave=os.path.join(output_dir, demProj_Slave), 
                           outPath=fine_gird_path)

    list_of_Grids.append(fine_gird_path)


    ## SARCoRegistration Application (changeo step) ##
    print("\n SARCoRegistration Application \n")
    slave_Image_CoRe = os.path.splitext(slave_Image_base)[0] + "_coregistrated.tif"
    if "slave_CoRe_Name" in param :
        slave_Image_CoRe = param['slave_CoRe_Name']
        slave_Image_CoRe += "?&gdal:co:TILED=YES"

    coRe_path = os.path.join(output_dir, slave_Image_CoRe)
    coRegistrationApp(insarmaster=master_Image, 
                      insarslave=slave_Image,
                      ingrid=fine_gird_path, doppler0=doppler0, 
                      outPath=coRe_path)



    ## SARRobustInterferogram Application (interf step) ##
    if with_interferogram :
        print("\n SARRobustInterferogram Application \n")
        interferogram_path = os.path.join(output_dir, "interferogram.tif")

        interferogramApp(insarmaster=master_Image, 
                         incoregistratedslave=os.path.join(output_dir, slave_Image_CoRe), 
                         insarslave=slave_Image, 
                         ingrid=fine_gird_path, 
                         incartmeanmaster=os.path.join(master_dir, master_cartesian_mean), 
                         outPath=interferogram_path)

        list_of_Interferograms.append(interferogram_path)

    # Output lists
    return list_of_Grids, list_of_Interferograms


def gridToInterferogram_S1IW(dem, master_Image, master_Image_base, slave_Image, slave_Image_base, master_dir, slave_dir, param, output_dir):
    """
        Main function for DInSAR processing and S1IW mode
    
        This function applies several processing to provide the result interferogram (with concatenation of all interferograms)
    
        :param dem: Input DEM  
        :param master_Image: Master image name (with path included) 
        :param master_Image_base: Master image name   
        :param slave_Image: Slave image name (with path included) 
        :param slave_Image_base: Slave image name
        :param master_dir: Output directory (to store result on master image only)
        :param slave_dir: Output directory (to store result on slave image only)
        :param param: Dictionary to retrieve all parameters used into this processing chain 
        :param output_dir: Output directory 

        :type dem: string 
        :type master_Image: string 
        :type master_Image_base: string
        :type slave_Image: string
        :type slave_Image_base: string
        :type master_dir: string
        :type slave_dir: string
        :type param: dict
        :type output_dir: string

        :return: list of grids and interferograms
        :rtype: list, list 
    """    
    # Get elements into param dictionnaries 
    ml_azimut = param['ml_azimut']
    ml_range = param['ml_range']
    validBurstMaster = param['validBurstMaster']
    validBurstSlave = param['validBurstSlave']
    ml_geoGrid_azimut = param['ml_geoGrid_azimut']
    ml_geoGrid_range = param['ml_geoGrid_range']
    geoGrid_gridstep_range = param['geoGrid_gridstep_range']
    geoGrid_gridstep_azimut = param['geoGrid_gridstep_azimut']    
    geoGrid_threshold = param['geoGrid_threshold']
    geoGrid_gap = param['geoGrid_gap']
    doppler0 = param['doppler0']
    gain_interfero = param['gain_interfero']
    advantage = param['advantage']
    esd_NbIter = param['esd_NbIter']
    esd_AutoMode = param['esd_AutoMode']

    with_interferogram = True
    with_concatenate = True

    if "with_interferogram" in param :
        with_interferogram = func_utils.str2bool(param['with_interferogram'])

    if "with_concatenate" in param :
        with_concatenate = func_utils.str2bool(param['with_concatenate'])


    nbramps = 256*2*10+1
    
    # define applications functions with the previous parameters
    fineGridDeformationApp = partial(diapOTBApp.fineGridDeformation, indem=dem, mlran=ml_geoGrid_range, mlazi=ml_geoGrid_azimut, gridsteprange=geoGrid_gridstep_range, gridstepazimut=geoGrid_gridstep_azimut, 
                                     threshold=geoGrid_threshold, gap=geoGrid_gap, advantage=advantage)
    
    coRegistrationApp = partial(diapOTBApp.coRegistration, gridsteprange=geoGrid_gridstep_range, gridstepazimut=geoGrid_gridstep_azimut, nbramps=nbramps)

    derampApp = partial(diapOTBApp.deramp, gridsteprange=geoGrid_gridstep_range, gridstepazimut=geoGrid_gridstep_azimut, reramp="true", shift="true")

    interferogramApp = partial(diapOTBApp.interferogram, gridsteprange=geoGrid_gridstep_range, gridstepazimut=geoGrid_gridstep_azimut, mlran=1, mlazi=1, gain=gain_interfero)
    
    esdApp = partial(diapOTBApp.esd, insar=master_Image) 

    esdCorrectInterferogram = partial(esd_correct_interferogram, validBurstMaster, validBurstSlave, master_Image_base, slave_Image_base, geoGrid_gridstep_range, geoGrid_gridstep_azimut, gain_interfero, slave_dir, master_dir, doppler0)

    # Empty lists
    list_of_Grids = []
    list_of_Interferograms = []
    list_of_slaveCorReramp = []
    list_of_masterReramp = []

    for id_loop in range(0, len(validBurstMaster)):
        burstId = validBurstMaster[id_loop]
        burstId_slave = validBurstSlave[id_loop]
        
        print("\n BurstId = " + str(burstId) + "\n")
      
        burst_dir = os.path.join(slave_dir, "burst" + str(burstId))
        Master_burst_dir = os.path.join(master_dir, "burst" + str(burstId))
        
        burstM = os.path.splitext(master_Image_base)[0] + "_burst" + str(burstId) + ".tif"
        burstS = os.path.splitext(slave_Image_base)[0] + "_burst" + str(burstId_slave) + ".tif"
        
        burstDerampM = os.path.splitext(master_Image_base)[0] + "_burst" + str(burstId) + "_deramp" + ".tif"
        burstDerampS = os.path.splitext(slave_Image_base)[0] + "_burst" + str(burstId_slave) + "_deramp" + ".tif"

        master_Image_ML = os.path.splitext(master_Image_base)[0] + "_burst" + str(burstId) + "_ml" + str(ml_azimut) + str(ml_range) + ".tif"
        slave_Image_ML = os.path.splitext(slave_Image_base)[0] + "_burst" + str(burstId_slave) + "_ml" + str(ml_azimut) + str(ml_range) + ".tif"
        

        demProj_Master = "demProj" +  "_burst" + str(burstId) +"_Master.tif"
        demProj_Slave = "demProj" +  "_burst" + str(burstId_slave) + "_Slave.tif"
        master_cartesian_mean = "CartMeanMaster" +  "_burst" + str(burstId) + ".tif"
    

        ## SARFineDeformationGrid Application ##
        print("\n SARFineDeformationGrid Application \n")
        fine_grid = "fineDeformationGrid"+ "_burst" + str(burstId) + ".tif"
        fine_gird_path = os.path.join(burst_dir, fine_grid)
        fineGridDeformationApp(insarmaster=os.path.join(Master_burst_dir, burstDerampM), 
                               insarslave=os.path.join(burst_dir, burstDerampS),
                               inmlmaster=os.path.join(Master_burst_dir, master_Image_ML), 
                               inmlslave=os.path.join(burst_dir, slave_Image_ML), 
                               indemprojmaster=os.path.join(Master_burst_dir, demProj_Master), 
                               indemprojslave=os.path.join(burst_dir, demProj_Slave), 
                               outPath=fine_gird_path)

        list_of_Grids.append(os.path.join(burst_dir, fine_grid))


        ## SARCoRegistration Application (changeo step) ##
        print("\n SARCoRegistration Application \n")
        slave_Image_CoRe = os.path.splitext(slave_Image_base)[0] + "_burst" + str(burstId) + "_coregistrated.tif"
        coRe_path = os.path.join(burst_dir, slave_Image_CoRe)

        print("\n Dopller0 :")
        dop0Val = doppler0[burstId-validBurstMaster[0]]
        print(dop0Val)
        print("\n")

        coRegistrationApp(insarmaster=os.path.join(Master_burst_dir, burstDerampM), 
                          insarslave=os.path.join(burst_dir, burstDerampS),
                          ingrid=fine_gird_path, doppler0=doppler0[burstId-validBurstMaster[0]], 
                          outPath=coRe_path)

        
        list_of_masterReramp.append(os.path.join(Master_burst_dir, burstDerampM))

        ## SARDeramp Application (reramp mode) ##
        print("\n Deramping Application \n")
        # Slave (CoRegistrated)
        burstCoReRerampS = os.path.splitext(slave_Image_base)[0] + "_burst" + str(burstId) + "_coregistrated_reramp" + ".tif"
        deramp_Path = os.path.join(burst_dir, burstCoReRerampS)
        derampApp(inImg=os.path.join(burst_dir, slave_Image_CoRe), ingrid=fine_gird_path, 
                  inslave=os.path.join(burst_dir, burstDerampS), outPath=deramp_Path)

        list_of_slaveCorReramp.append(deramp_Path)
        
        ## SARRobustInterferogram Application (interf step) ##
        if with_interferogram :
            print("\n SARRobustInterferogram Application \n")
            interferogram_path = os.path.join(burst_dir, "interferogram" + "_burst" + str(burstId) + "_iter" + str(0) + ".tif")
            if esd_NbIter > 0 :
                esd_dir = os.path.join(burst_dir, "esd")

                if not os.path.exists(esd_dir):
                    os.makedirs(esd_dir)

                interferogram_path = os.path.join(esd_dir, "interferogram" + "_burst" + str(burstId) + "_iter" + str(0) + ".tif")

            interferogramApp(insarmaster=os.path.join(Master_burst_dir, burstM), 
                             incoregistratedslave=deramp_Path, insarslave=os.path.join(burst_dir, burstS), 
                             ingrid=fine_gird_path, 
                             incartmeanmaster=os.path.join(Master_burst_dir, master_cartesian_mean), 
                             outPath=interferogram_path)

            list_of_Interferograms.append(interferogram_path)

    ### ESD Loop (if required) ####
    print ("Beginning of ESD Loop")

    # Beginning of ESD Loop (only if with_interferogram)
    if with_interferogram :
        firstBurst = validBurstMaster[0]
        lastBurst = validBurstMaster[len(validBurstMaster)-1]

        if esd_NbIter > 0 :

            azimut_shift_esd = []
            azimut_shift_esd_global =  [0.] * (lastBurst-firstBurst)

            for iter_esd in range(1, esd_NbIter+1) :

                print ("Iteration number = {number}".format(number=iter_esd))

                # Clear all azimut shifts
                azimut_shift_esd[:] = []

                for id_loop in range(0, len(validBurstMaster)):
                    burstId = validBurstMaster[id_loop]

                    print("\n BurstId = " + str(burstId) + "\n")

                    # Paths
                    burst_dir = os.path.join(slave_dir, "burst" + str(burstId))
                    esd_dir = os.path.join(burst_dir, "esd")
                    esd_path = os.path.join(esd_dir, "esdOut" + "_burst" + str(burstId) + "_iter" + str(0) + ".tif")

                    # Check number of burst index (not SARESD for lastBurst)
                    if burstId < lastBurst :
                        ## SARESD Application ##
                        print("\n ESD Application \n")
                        aziShiftEsd = esdApp(ininterfup=list_of_Interferograms[burstId-firstBurst], 
                                             ininterflow=list_of_Interferograms[burstId-firstBurst+1], 
                                             burstIndex=burstId, outPath=esd_path)


                        print("aziShift = " + str(aziShiftEsd))
                        azimut_shift_esd.append(aziShiftEsd)

                # Correction of Interferogram with the esd shift
                # new reference => new list of interferograms
                list_of_Interferograms = esdCorrectInterferogram(iter_esd, list_of_Grids, azimut_shift_esd, 
                                                                 azimut_shift_esd_global)  

                # Check azimuth shift to end esd loop if automatic mode enabled
                if esd_AutoMode : 
                    absMax_aziShift = abs(max(azimut_shift_esd_global, key=abs))
                    if absMax_aziShift < 0.01 :
                        break
    

    # Concatenate interferograms
    ## SARConcatenateBursts (for interferograms) ##
    if with_interferogram and with_concatenate :
        print("\n SARConcatenateBursts Application (for interferograms) \n")
        interferogram_swath = "interferogram_swath.tif"
        diapOTBApp.concatenate(list_of_Interferograms, master_Image, firstBurst, os.path.join(output_dir, interferogram_swath))

    # Output lists
    return list_of_Grids, list_of_Interferograms, list_of_slaveCorReramp, list_of_masterReramp
