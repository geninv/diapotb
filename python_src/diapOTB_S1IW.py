#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

""" 
    diapOTB_S1IW.py
    ===============

    Interferometry chain between two SAR images (master/slave) for S1 IW products
 
"""

__author__ = "POC-THALES"
__version__ = "0.1"
__status__ = "Developpement"
__date__ = "11/12/2018"
__last_modified__ = "05/02/2020"

# Imports
import logging
import os
import sys
import argparse
import re

from processings import Pre_Processing
from processings import Ground
from processings import DInSar

from utils import func_utils

# Main
if __name__ == "__main__":
    
    # Check arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("configfile", help="input conguration file for the application DiapOTB")
    args = parser.parse_args()
    print(args.configfile)

    func_utils.init_logger()
       
    dataConfig = func_utils.load_configfile(args.configfile, "S1_IW")

      # Get dictionaries
    dict_Global = dataConfig['Global']
    dict_PreProcessing = dataConfig['Pre_Processing']
    dict_Ground = dataConfig['Ground']
    dict_DInSAR = dataConfig['DIn_SAR']
   
         
    # Get elements from dictionaries
    # Global
    master_Image = dict_Global['in']['Master_Image_Path']
    slave_Image =  dict_Global['in']['Slave_Image_Path']
    dem =  dict_Global['in']['DEM_Path']
    output_dir = dict_Global['out']['output_dir']
    
    # Pre_Processing
    ml_range = dict_PreProcessing['parameter']['ML_range'] 
    ml_azimut = dict_PreProcessing['parameter']['ML_azimut']
    ml_gain = dict_PreProcessing['parameter']['ML_gain']
    dop_file = dict_PreProcessing['out']['doppler_file']
    
    # Ground
    
    # DIn_SAR
    geoGrid_gridstep_range = dict_DInSAR['parameter']['GridStep_range']
    geoGrid_gridstep_azimut = dict_DInSAR['parameter']['GridStep_azimut']
    geoGrid_threshold = dict_DInSAR['parameter']['Grid_Threshold']
    geoGrid_gap = dict_DInSAR['parameter']['Grid_Gap']
    ml_geoGrid_range = ml_range
    ml_geoGrid_azimut = ml_azimut
    gain_interfero = dict_DInSAR['parameter']['Interferogram_gain']
    # esd loop 
    esd_AutoMode = False # automatic mode to apply a threshold inside the esd loop
    esd_NbIter = 0
    
    if 'ESD_iter' in dict_DInSAR['parameter']:
        esd_NbIter = dict_DInSAR['parameter']['ESD_iter']
        if not isinstance(esd_NbIter, int) :
            esd_AutoMode = True
            esd_NbIter = 10 # 10 iterations maximum for automatic mode
    else :
        esd_AutoMode = True
        esd_NbIter = 10 # 10 iterations maximum for automatic mode
   

    # Check Threshold
    if (geoGrid_threshold < 0) or (geoGrid_threshold > 1) :
        func_utils.log(logging.CRITICAL, "Error, Wrong Threshold for fine deformation grid")
        geoGrid_threshold = 0.3

    # Check if images/dem exist
    if not os.path.exists(master_Image) :
        func_utils.log(logging.CRITICAL, "Error, {img} does not exist. Check its path.".format(img=master_Image))
        quit()
    if not os.path.exists(slave_Image) :
        func_utils.log(logging.CRITICAL, "Error, {img} does not exist. Check its path.".format(img=slave_Image))
        quit()
    if not os.path.exists(dem) :
        func_utils.log(logging.CRITICAL, "Error, {img} does not exist. Check its path.".format(img=dem))
        quit()
    if not os.path.exists(output_dir):
        print("The output directory does not exist and will be created")
        os.makedirs(output_dir)
    else :
        print("The output directory exists. Some files can be overwritten")

    
    # Init file handler (all normaly print on std is redirected into info.log) 
    # To use previous print on std, use printOnStd
    func_utils.init_fileLog(output_dir) 

    # Recap of input parameter into info.log
    func_utils.log(logging.INFO, "########### Input Parameters for the current execution ############## ")
    func_utils.log(logging.INFO, " Pre_Processing : ")
    func_utils.log(logging.INFO, "ml_range : {param}".format(param=ml_range))
    func_utils.log(logging.INFO, "ml_azimut : {param}".format(param=ml_azimut))
    func_utils.log(logging.INFO, "ml_gain : {param}".format(param=ml_gain))
    func_utils.log(logging.INFO, "dop_file : {param}".format(param=dop_file))

    # DIn_SAR
    func_utils.log(logging.INFO, " DIn_SAR : ")
    func_utils.log(logging.INFO, "geoGrid_gridstep_range : {param}".format(param=geoGrid_gridstep_range))
    func_utils.log(logging.INFO, "geoGrid_gridstep_azimut : {param}".format(param=geoGrid_gridstep_azimut))
    func_utils.log(logging.INFO, "geoGrid_threshold : {param}".format(param=geoGrid_threshold))
    func_utils.log(logging.INFO, "geoGrid_gap : {param}".format(param=geoGrid_gap))
    func_utils.log(logging.INFO, "ml_geoGrid_range : {param}".format(param=ml_geoGrid_range))
    func_utils.log(logging.INFO, "ml_geoGrid_azimut : {param}".format(param=ml_geoGrid_azimut))
    func_utils.log(logging.INFO, "gain_interfero : {param}".format(param=gain_interfero))
    func_utils.log(logging.INFO, "esd_AutoMode : {param}".format(param=esd_AutoMode))
    func_utils.log(logging.INFO, "esd_NbIter : {param}".format(param=esd_NbIter))

    func_utils.log(logging.INFO, "########### Input Images for the current execution ############## ")
    func_utils.log(logging.INFO, "master_Image : {param}".format(param=master_Image))
    func_utils.log(logging.INFO, "slave_Image : {param}".format(param=slave_Image))
    func_utils.log(logging.INFO, "dem : {param}".format(param=dem))

    # check Burst index      
    master_Image_base = os.path.basename(master_Image)
    slave_Image_base = os.path.basename(slave_Image)
    
    # Retrieve some information about our input images
    dictKWLMaster = func_utils.getImageKWL(master_Image)
    dictKWLSlave = func_utils.getImageKWL(slave_Image)

    # Check header version
    if int(dictKWLMaster['header.version']) < 3 or int(dictKWLSlave['header.version']) < 3 :
        func_utils.log(logging.CRITICAL, "Error, Upgrade your geom file")
        quit()

    # Get information about DEM (spacing, size ..)
    dictDEMInformation = func_utils.getDEMInformation(dem)

    # Choose advantage for correlation or projection according to DEM resolution
    advantage = "projection" # By default projection
    
    if dictDEMInformation['estimatedGroundSpacingXDEM'] > 40. or dictDEMInformation['estimatedGroundSpacingYDEM'] > 40. :
        advantage = "correlation" # Correlation if resolution > 40 m
        func_utils.log(logging.WARNING, "Resolution of the input DEM is inferior to 40 meters : A correlation will be used to correct all deformation grids")
        

    # Check the index of bursts
    minNbBurst = min([int(dictKWLMaster['support_data.geom.bursts.number']), int(dictKWLSlave['support_data.geom.bursts.number'])])

    firstBurst = 0
    lastBurst = minNbBurst
    burstIndexOk = True

    try:
        if 'parameter' in dict_Global:
            if 'burst_index' in dict_Global['parameter']:
                burstList = dict_Global['parameter']['burst_index'].split('-');
                burstList = [int(i) for i in burstList]
            
                if len(burstList) == 2 :
                    firstBurst = min(burstList)
                    lastBurst = max(burstList)
    except Exception as err:
        func_utils.log(logging.CRITICAL, "Error, Wrong burst index")
        quit()
                
    if minNbBurst < firstBurst or minNbBurst < lastBurst or lastBurst < 0 or firstBurst < 0 :
        func_utils.log(logging.CRITICAL,"Error, Wrong burst index")
        quit()
   
    
    nbBurstSlave = int(dictKWLSlave['support_data.geom.bursts.number'])
    validBurstMaster, validBurstSlave = func_utils.selectBurst(dictKWLMaster, dictKWLSlave, firstBurst, lastBurst, nbBurstSlave)

  
    if len(validBurstMaster) == 0 or len(validBurstSlave) == 0 :
        func_utils.log(logging.CRITICAL, "Error, Wrong burst index (slave index does not match with master index)")
        quit()


    # Create directory for each burst
    for burstId in range(validBurstMaster[0], validBurstMaster[len(validBurstMaster)-1]+1):
        if not os.path.exists(os.path.join(output_dir, "burst" + str(burstId))):
            os.makedirs(os.path.join(output_dir, "burst" + str(burstId)))


    func_utils.printOnStd("\n Beginning of DiapOTB processing (S1 IW mode) \n")
    func_utils.log(logging.INFO, "############ Beginning of DiapOTB processing (S1 IW mode) ##############")
                        
    ####################### Pre Processing Chain ##########################
    # Master
    func_utils.printOnStd("\n Master Pre_Processing chain \n")
    func_utils.log(logging.INFO, "Master Pre_Processing Application")

    paramPreMaster = {}
    paramPreMaster['ml_range'] = ml_range
    paramPreMaster['ml_azimut'] = ml_azimut
    paramPreMaster['ml_gain'] = ml_gain
    paramPreMaster['dop_file'] = dop_file
    paramPreMaster['validBurstMaster'] = validBurstMaster

    results_PreProM = Pre_Processing.extractToMultilook(master_Image, master_Image_base, paramPreMaster, "S1_IW",
                                                        output_dir)
    
    dop0Master = results_PreProM[0]
     
    # Slave
    func_utils.printOnStd("\n Slave Pre_Processing chain \n")
    func_utils.log(logging.INFO, "Slave Pre_Processing Application")
   
    paramPreSlave = {}
    paramPreSlave['ml_range'] = ml_range
    paramPreSlave['ml_azimut'] = ml_azimut
    paramPreSlave['ml_gain'] = ml_gain
    paramPreSlave['dop_file'] = dop_file
    paramPreSlave['validBurstMaster'] = validBurstMaster
    paramPreSlave['validBurstSlave'] = validBurstSlave

    results_PreProS = Pre_Processing.extractToMultilook(slave_Image, slave_Image_base, paramPreSlave, "S1_IW", 
                                                       output_dir)
   
    dop0Slave = results_PreProS[0]

    ######################### Ground Chain #############################
    # Master
    func_utils.printOnStd("\n Master Ground chain \n")
    func_utils.log(logging.INFO, "Master Ground Application")
 
    paramGroundMaster = {}
    paramGroundMaster['nodata'] = -32768
    paramGroundMaster['withxyz'] = "true"
    paramGroundMaster['validBurstMaster'] = validBurstMaster
    
    Ground.demProjectionAndCartesianEstimation(master_Image, master_Image_base, dem, paramGroundMaster, 
                                               "S1_IW", output_dir)
    

    # Slave
    func_utils.printOnStd("\n Slave Ground chain \n")
    func_utils.log(logging.INFO, "Slave Ground Application")

    paramGroundSlave = {}
    paramGroundSlave['nodata'] = -32768
    paramGroundSlave['withxyz'] = "true"
    paramGroundSlave['validBurstMaster'] = validBurstMaster
    paramGroundSlave['validBurstSlave'] = validBurstSlave
    
    Ground.demProjectionAndCartesianEstimation(slave_Image, slave_Image_base, dem, paramGroundSlave, 
                                               "S1_IW", output_dir)


    ######################## DIn_SAR Chain #############################
    func_utils.printOnStd("\n DIn_SAR chain \n")
    func_utils.log(logging.INFO, "DIn_SAR chain")

    counter = 0
    list_of_Interferograms = []

    # Create param
    param = {}
    param['ml_azimut'] = ml_azimut
    param['ml_range'] = ml_range
    param['validBurstMaster'] = validBurstMaster
    param['validBurstSlave'] = validBurstSlave
    param['ml_geoGrid_azimut'] = ml_geoGrid_azimut
    param['ml_geoGrid_range'] = ml_geoGrid_range
    param['geoGrid_gridstep_range'] = geoGrid_gridstep_range
    param['geoGrid_gridstep_azimut'] = geoGrid_gridstep_azimut
    param['geoGrid_threshold'] = geoGrid_threshold
    param['geoGrid_gap'] = geoGrid_gap
    param['doppler0'] = dop0Slave
    param['gain_interfero'] = gain_interfero
    param['advantage'] = advantage
    param['esd_NbIter'] = esd_NbIter 
    param['esd_AutoMode']  = esd_AutoMode

    DInSar.gridToInterferogram(dem, master_Image, master_Image_base, slave_Image, slave_Image_base, output_dir, output_dir, param, 'S1_IW', output_dir)
 
    func_utils.printOnStd("\n End of DiapOTB processing (S1 IW mode) \n")
    func_utils.log(logging.INFO, "############ End of DiapOTB processing  (S1 IW mode) ##############")
