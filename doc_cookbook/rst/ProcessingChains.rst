
Processing Chains
=================

Most of DiapOTB applications have been used into larger processing workflows. 

All processing chains build a final interferogram from two SLC (Single Look Complex) images and a digital elevation model.

Two kinds of workflows are available according input products and modes :

.. toctree::
	:maxdepth: 1

	ProcessingChains/diapOTB_Chain.rst
	ProcessingChains/diapOTB_S1IW_Chain.rst

At the end, a final interferogram is created as a VectorImage into Master geometry  :

- Band 1 : Amplitude
- Band 2 : Phase
- Band 3 : Coherence
- Band 4 : IsData boolean to indicate if output pixels are consistent


Example : Output interfergram for S1 SM product (Reunion island)

.. image:: ../Art/results_DiapOTB/Results_Reunion.png
