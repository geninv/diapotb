.. _diapOTB_S1IW_Chain:

For S1 IW mode
==============

This workflow works on S1 IW data.

Description
-----------

This chain is divided into three parts descrided below :

- Pre-Processing Chain
- Ground Chain
- DIn-SAR Chain


Launch this chain
-----------------

A Python script called diapOTB_S1IW.py spreads the general processing chain (diapOTB.py)
The input configuration file has the following organization :

.. image:: ../../Art/conf_chains/diapOTB_2_chain.png

Four sections compose the configuration file :

- Global :

  - **Master_Image_Path :** Path to the Master SAR Image. 
  - **Slave_Image_Path :** Path to the Master SAR Image
  - **DEM_Path :** Path to the DEM
  - **output_dir :** Output directory (all images or files created by DiapOTB will be stored inside this directory)
  - **burst_index :** burst index selected form the Master Image

- Pre_Processing :

  - **doppler_file :** Output file to store Doppler0 result
  - **ML_range :** MultLook factor on range dimension
  - **ML_azimut :** MultLook factor on azimut dimension
  - **ML_gain :** Gain applied on MultiLooked images


- Ground :


- DIn_SAR :

  - **GridStep_range :** Step for the deformation grid on range dimension
  - **GridStep_azimut :** Step for the deformation grid on azimut dimensio
  - **GridStep_Threshold :** Threshold for the correlation grid (applied on correlation rate)
  - **Grid_Gap :** Maximum gap between DEM grid values and the mean value. If the difference betwwen a value of the DEM grid ans the mena is superior to this gap then the value is set to the mean. (Avoid incoherent shift)
  - **Interferogram_gain  :** Gain applied on amplitude band of output inteferegram.
  - **Interferogram_ortho  :** Boolean to activate the optionnal ouput : inteferogram into ground geometry (By default false)
  - **ESD_iter :** Number of iterations for ESD loop. An automatic mode is possible by passing "auto" as value for this parameter.


The processing chain needs metadata to launch the applications. Thus the path to the input images must be into the native directory for each kind of products (i.e SAFE directory for Sentinel-1 products).
For that moment the ground geometry for interferogram does not provide satisfactory results.
