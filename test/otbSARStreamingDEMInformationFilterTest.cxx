/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdlib>
#include "itkMacro.h"
#include "otbImage.h"
#include "otbImageFileReader.h"
#include "otbSARStreamingDEMInformationFilter.h"

#include <complex>

int otbSARStreamingDEMInformationFilterTest(int argc, char * argv[])
{
  // Verify the number of parameters in the command line
  if (argc != 3)
    {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " inputDEMFile inputSARImageFile" << std::endl;
    return EXIT_FAILURE;
    }

  // Image types
  typedef otb::Image<float, 2> ImageType;
  typedef otb::Image <std::complex<float>, 2>       ImageSARInType ;

  // Readers
  typedef otb::ImageFileReader<ImageType> ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  typedef otb::ImageFileReader <ImageSARInType > ReaderComplexType ;
  ReaderComplexType::Pointer reader_SAR = ReaderComplexType::New();
  
  // Filter
  typedef otb::SARStreamingDEMInformationFilter<ImageType> FilterType;
  FilterType::Pointer demFilter = FilterType::New();
  
  
  // Define pipelines
  reader->SetFileName(argv[1]);  
  reader_SAR->SetFileName(argv[2]);
  
  // First Pipeline (read SAR metadata
  ImageSARInType::Pointer SARPtr = reader_SAR->GetOutput();
  reader_SAR->UpdateOutputInformation();
  
  demFilter->SetSARImageKeyWorList(SARPtr->GetImageKeywordlist());

    // Main Pipeline 
  demFilter->SetInput(reader->GetOutput());
 
  // Execute pipeline
  try
    {
      demFilter->Update();
      std::cout << demFilter->GetSide_nbCol_nbLines() << std::endl;
      std::cout << demFilter->GetSide_0_0() << std::endl;
      double gain; 
      int directionDEM_forlines, directionDEM_forcolunms;
      demFilter->GetDEMInformation(gain, directionDEM_forlines, directionDEM_forcolunms);
      std::cout << gain << std::endl;
      std::cout << directionDEM_forlines << std::endl;
      std::cout << directionDEM_forcolunms << std::endl;
    }
  catch (itk::ExceptionObject& err)
    {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

