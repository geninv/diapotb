/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdlib>
#include "itkMacro.h"
#include "otbImage.h"
#include "otbImageFileReader.h"
#include "otbImageFileWriter.h"
#include "otbSARQuadraticAveragingImageFilter.h"

// Direction
enum class DirectionType {RANGE=0,AZIMUTH=1};

int otbSARQuadraticAveragingImageFilterTest(DirectionType direction, int argc, char * argv[])
{
  // Verify the number of parameters in the command line
  if (argc != 3)
    {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " inputImageFile outputImageFile" << std::endl;
    return EXIT_FAILURE;
    }

  // Image type
  const unsigned int Dimension = 2;
  typedef unsigned int             PixelType;
  typedef otb::Image<PixelType, Dimension> ImageType;

  // Reader
  typedef otb::ImageFileReader<ImageType> ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(argv[1]);

  // Filter
  typedef otb::SARQuadraticAveragingImageFilter<ImageType> FilterType;
  FilterType::Pointer filter = FilterType::New();
  filter->SetAveragingFactor(3);
  FilterType::DirectionType directionFilter = (direction == DirectionType::AZIMUTH) ? FilterType::DirectionType::AZIMUTH : FilterType::DirectionType::RANGE ; 
  filter->SetDirection(directionFilter);

  std::cout<<"Print Averaging filter information: "<< std::endl;
  std::cout << filter << std::endl;

  // Writer
  typedef otb::ImageFileWriter<ImageType> WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetFileName(argv[2]);

  // Define pipeline
  filter->SetInput(reader->GetOutput());
  writer->SetInput(filter->GetOutput());

  // Execute pipeline
  try
    {
    writer->Update();
    }
  catch (itk::ExceptionObject& err)
    {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

int otbSARQuadraticAveragingImageFilterInAzimuthTest(int argc, char * argv[])
{
  int result = otbSARQuadraticAveragingImageFilterTest(DirectionType::AZIMUTH,argc,argv);
  return result;
}

int otbSARQuadraticAveragingImageFilterInRangeTest(int argc, char * argv[])
{
  int result = otbSARQuadraticAveragingImageFilterTest(DirectionType::RANGE,argc,argv);
  return result;
}

