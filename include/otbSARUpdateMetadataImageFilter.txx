/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARUpdateMetadataImageFilter_txx
#define otbSARUpdateMetadataImageFilter_txx

#include "otbSARUpdateMetadataImageFilter.h"

#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"

#include <cmath>
#include <algorithm>

namespace otb
{
   /**
   * Print
   */
  template<class TImage>
  void
  SARUpdateMetadataImageFilter< TImage >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);

    os << indent << "Precise first line time  : " << m_TimeFirstLine << std::endl;
    os << indent << "Precise Slant range : " << m_SlantRange << std::endl;
  }

  
  /**
   * Method GenerateOutputInformaton()
   **/
  template<class TImage>
  void
  SARUpdateMetadataImageFilter< TImage >
  ::GenerateOutputInformation()
  {
    // Call superclass implementation
    Superclass::GenerateOutputInformation();

    // Get Input and Output ptr
    ImagePointer outputPtr = this->GetOutput();
    ImagePointer inputPtr = const_cast< ImageType * >(this->GetInput());
        
    // Set new keyword list to output image with precise metadata
    ImageKeywordlist outputKWL = inputPtr->GetImageKeywordlist();
    //outputKWL.SetMetadataByKey("support_data.first_line_time", m_TimeFirstLine);
    //outputKWL.SetMetadataByKey("support_data.slant_range_to_first_pixel", m_SlantRange);

    std::ostringstream str_slantRange;
    str_slantRange.precision(14);
    str_slantRange << std::fixed << m_SlantRange;

    std::cout << "str_slantRange = " << str_slantRange.str() << std::endl;

    outputKWL.AddKey("support_data.first_line_time", m_TimeFirstLine);
    outputKWL.AddKey("support_data.slant_range_to_first_pixel", str_slantRange.str());
    outputPtr->SetImageKeywordList(outputKWL);     
  }

  
  template<class TImage>
  void
  SARUpdateMetadataImageFilter< TImage >
  ::ThreadedGenerateData(const ImageRegionType& outputRegionForThread, itk::ThreadIdType /*threadId*/)
  {
    // Get Input and Output ptr
    ImagePointer outputPtr = this->GetOutput();
    ImagePointer inputPtr = const_cast< ImageType * >(this->GetInput());

    // Same region
    ImageRegionType inputRegion = outputRegionForThread;
     
    // Iterators
    typedef itk::ImageRegionConstIterator<ImageType> InItType;
    typedef itk::ImageRegionIterator<ImageType> OutItType;

    InItType InIt(inputPtr, inputRegion);
    InIt.GoToBegin();

    OutItType OutIt(outputPtr, outputRegionForThread);
    OutIt.GoToBegin();

    // Copy input image into output
    while (!InIt.IsAtEnd())
      {
	OutIt.Set(InIt.Get());

	++OutIt; 
	++InIt;
      }
  }

} /*namespace otb*/

#endif
